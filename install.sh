#!/bin/bash
files="pandas numpy tensorflow joblib sklearn \
tqdm nltk tabulate sqlalchemy sqlalchemy_utils \
ftfy spacy psycopg2"
for f in $files
do
	pip3 install $f
done
python3 -m spacy download en_core_web_sm
sudo apt install postgresql -y

#!/bin/bash

# exit at the very first failing command
set -e
set -o pipefail

mypath=`realpath $0`
mybase=`dirname $mypath`
source $mybase/setup/utils.bashrc

dumpfile=dump.sql.gz

function usage {
    echo "usage: $0 new|dump|restore"
    echo "  new: (re)create $db db, initialize facts schema from remote master"
    echo "  test: (re)create $db db with test data"
    echo "  dump: dump $db db into $dumpfile"
    echo "  restore: (re)create $db db from $dumpfile"
    echo
}

if [[ $# -ne 1 ]]; then
    usage
    exit 1
fi

cd $mybase

if [[ "$1" == 'new' ]]; then
    share_the_facts/db.sh new
    media_watcher/db.sh new
elif [[ "$1" == 'test' ]]; then
    share_the_facts/db.sh new
    media_watcher/db.sh test
elif [[ "$1" == 'restore' ]]; then
    dropdb --if-exists $db
    createdb $db
    gunzip --stdout $dumpfile | psql -f - $db
elif [[ "$1" == 'dump' ]]; then
    pg_dump --no-owner --encoding=UTF8 $db | gzip - > $dumpfile
else
    usage
    exit 1
fi

class Match: 
	def __init__(self, live_sentence, db_sentence, confidence, stf_id="", url="", organization_id="", rating="", ratingURL="", speaker="", title=""):
		self.live_sentence=live_sentence
		self.db_sentence=db_sentence
		self.stf_id=stf_id
		self.url=url
		self.organization_id=organization_id
		self.rating=rating
		self.ratingURL=ratingURL
		self.confidence=confidence
		self.speaker=speaker
		self.title=title
		self.speaker_with_title = f'{self.speaker.strip()}, {self.title}' if self.title!='' else self.speaker.strip()
		self._organizations = {1:"Washington Post",15:"Newsweek",14:"The Ferret",10:"Climate Feedback",31:"Verafiles",2:"PolitiFact",37:"BoomLive",3:"FactCheck.org"}
		self.organization = self._organizations[self.organization_id] if organization_id!="" else ""
	def toDict(self):
		return {"live_sentence":self.live_sentence,"db_sentence":self.db_sentence,"url":self.url,"organization":self.organization,"rating":self.rating,"speaker_with_title":self.speaker_with_title,"confidence":self.confidence}

	def __str__(self):
		return f'{self.live_sentence}, {self.db_sentence}: {self.confidence}'



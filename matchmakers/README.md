## Matchmakers

All models implement the `Matchmaker` abstract base class, which can be found in `engine/common/`. `Matchmaker` objects are required to implement the following methods: 
 - `trainModel` 
- `compareTwo`: Feeds two sentences into the model and returns a confidence score and prediction. 
- `matchSentence_db`: Given a sentence, this method compares the sentence to the whole database, and returns a list of `Match` objects (see below).
- `onFinish`: Any tasks which should be executed before the code exits--for example, pickling caches.
- All models also provide a way to load a saved model, but this is not standardized. 

The `Matchmaker` class also has a `testModel` method, which, given a csv of formatted test data, computes F1 scores for the model's performance on that test data. 

#### Match

All models return `Match` objects. `Match` objects are initialized with a live sentence, a sentence from the database, and a confidence score, along with optionally a Share the Facts id, an organization id, a rating, a URL to the fact check, a speaker, and the speaker's title.


#### FakeModel
As the name suggests this, is not a real model. Its `matchSentence_db` method pseudo-randomly returns between 0 and 2 matches with pseudo-random confidence scores, This class is useful for testing parts of the pipeline not related to the model because it finds "matches" instantaneously. 

#### NLPModel
`NLPModel` is a model based on NLP-generated sentence similarity scores that is an aggregate of similarity scores for each part of speech, roughly based on [this paper](https://www.sciencedirect.com/science/article/pii/S0166361515300531?via%3Dihub). It contains the `obj/` folder, which is where the pickled, trained models are stored, along with `idf.csv`, which weights words based on a tf-idf score in our database. 

#### NNModel
`NNModel` is a Manhattan Siamese long short-term memory neural network based on a [simplified implementation](https://medium.com/mlreview/implementing-malstm-on-kaggles-quora-question-pairs-competition-8b31b0b16a07) of [this paper](http://www.mit.edu/~jonasm/info/MuellerThyagarajan_AAAI16.pdf). It relies on pre-trained word embeddings (currently Google News word vectors) which can be loaded into local memory or called via a Flask web API (`nnmodel/word2vec`). The `nnmodel/saved_models` folder is where checkpointed models and 'best weights' models are stored, and the `nnmodel/obj` folder is where simple caches storing word embeddings are stored.  

#### TransformerModel
The transformer model is an implementation of the state-of-the-art transformer model, originally created [by Google](https://arxiv.org/pdf/1706.03762.pdf) and then used [by OpenAI](https://blog.openai.com/language-unsupervised/) to beat state-of-the-art scores in 12 of 15 NLP challenges. OpenAI [made their pretrained transformer public](https://github.com/openai/finetune-transformer-lm), but the code was specifically designed to use the transformer for the ROCStories Cloze Test. We made some modifications to a [fork](https://github.com/jamesmullenbach/transformer-lm) of the OpenAI repository that used the transformer for a similar MNLI-like dataset in order to use it on MNLI.  This is the only model which we have successfully trained on the MNLI dataset, and which can identify entailment/contradiction well. 

The original code (`transformer.py`) was written for onetime use, and can be found in the `transformermodel/transformer` folder; the `TransformerWrapper` is our adaptation of the fork above to the `Matchmaker` class. Saved model parameters are stored in `transformermodel/transformer/save`. 
 
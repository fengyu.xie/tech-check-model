import json
import gensim
from flask import Flask
from flask import request

app = Flask(__name__)


#localhost:5000/word2vec?word=dog
@app.route('/word2vec', methods=['GET'])
def word2vec():
    word=request.args.get('word', default='', type=str) #get param from url (ex. .../word2vec?word=dog => dog)
    if word in word2vec: 
    	return (json.dumps(word2vec[word].tolist()))
    else: 
    	return(json.dumps(None))

if __name__ == "__main__":
    #accessing google model vectors 
    word2vec = gensim.models.KeyedVectors.load_word2vec_format('./data/GoogleNews-vectors-negative300.bin', binary=True)
    print('loaded vectors')
    app.run()
import pandas as pd 
import sys
import csv
from neuralnetwork_utils import processData, save_obj

pd.options.display.max_columns = 10
pd.options.display.max_colwidth=75
pd.options.display.max_rows = 350
pd.options.display.width=230
root="C:\\Users\\Caroline Wang\\OneDrive\\Duke\\Data+\\techcheck\\engine\\matchmakers\\nnmodel\\"
########################
mnli_train=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\multinli_1.0\multinli_1.0_train.txt", sep='\t', error_bad_lines=False)
mnli_train=mnli_train[["gold_label","sentence1","sentence2"]]
mnli_train.columns=["gold_label","claim1","claim2"]
mnli_train['match']=mnli_train[['gold_label']].apply(lambda row: 0 if row['gold_label']=="neutral" else 1,axis=1)
del mnli_train["gold_label"]

mnli_devmatch=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\multinli_1.0\multinli_1.0_dev_matched.txt", sep='\t', error_bad_lines=False)
mnli_devmatch=mnli_devmatch[["gold_label","sentence1","sentence2"]]
mnli_devmatch.columns=["gold_label","claim1","claim2"]
mnli_devmatch['match']=mnli_devmatch[['gold_label']].apply(lambda row: 0 if row['gold_label']=="neutral" else 1,axis=1)
del mnli_devmatch["gold_label"]

mnli_devmismatch=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\multinli_1.0\multinli_1.0_dev_mismatched.txt", sep='\t', error_bad_lines=False)
mnli_devmismatch=mnli_devmismatch[["gold_label","sentence1","sentence2"]]
mnli_devmismatch.columns=["gold_label","claim1","claim2"]
mnli_devmismatch['match']=mnli_devmismatch[['gold_label']].apply(lambda row: 0 if row['gold_label']=="neutral" else 1,axis=1)
del mnli_devmismatch["gold_label"]


mnli_train['difficulty']="NA"
mnli_devmatch['difficulty']="NA"
mnli_devmismatch['difficulty']="NA"

mnli_train.to_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\mnli_train.csv")
mnli_devmatch.to_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\mnli_devmatch.csv")
mnli_devmismatch.to_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\mnli_devmismatch.csv")

print("mnli_train\n",mnli_train.head(5))
print("mnli_devmatch\n",mnli_devmatch.head(5))
print("mnli_devmismatch\n",mnli_devmismatch.head(5))

sys.exit(0)

########################
# with open(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\ppdb-2.0-s-phrasal", 'rb') as f:
#     rows=[line.decode('utf-8') if '\\ x' not in line or 'xc3' not in line for line  in f]
#     print(len(rows))
#     ppdb_phrasal=pd.DataFrame(rows)
    # for line in f:

    #     line = line.decode('utf-8')
    #     # discard lines with unrecoverable encoding errors
    #     if '\\ x' in line or 'xc3' in line:
    #         continue
    #     fields = line.split('|||')

print(ppdb_phrasal.head(10))
sys.exit(0)
########################
twitter_train=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\twitter-train.data",sep='\t')

assert twitter_train.shape==(13062,7)
#match is in tuple format, (a,b), where a+b=5 is an invariant
#(0,5) is a difficult N; (5,0) is P (actually I would only consider (0,5) as negative examples)

twitter_test=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\twitter-test.data",sep='\t')
assert twitter_test.shape==(971,7)
#match is an integer from 0-5--possibly a? 

twitter_dev=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\twitter-dev.data",sep='\t')
assert twitter_dev.shape==(4726,7)
#same format as twitter_train

dframes=[twitter_train,twitter_test,twitter_dev]

# print(twitter_test.iloc[:,2:5].head(500))
# print(twitter_test.iloc[:,4].value_counts(dropna=False))
# print(twitter_dev.iloc[:,2:5].head(500))

# print(type(twitter_train.iloc[0,4]))
# twitter=(pd.concat(dframes)).iloc[:,2:5]
twitter=pd.concat(dframes)
print(twitter.shape)
# print(twitter.iloc[:,2:5].head(500))
sys.exit(0)
twitter.columns=['claim1','claim2','score_tuple']
twitter['match']=twitter[['score_tuple']].apply(lambda row: 0 if row['score_tuple']=="(0,5)" else 1,axis=1)
print(twitter.head(500))

sys.exit(0)
#####################
sick_train=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\SICK_train.txt",sep='\t')
sick_train=sick_train[['sentence_A','sentence_B','relatedness_score','entailment_judgment']]
sick_train.rename(columns={'entailment_judgment':'entailment_judgement'}, inplace=True)
# print(sick_train.columns)
print(sick_train[(sick_train["entailment_judgement"]=="NEUTRAL")&(sick_train["relatedness_score"]>=4)])
sys.exit(0)
#####################
semeval_train=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\en-train.txt",header=None,sep='\t')
semeval_test=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\en-test.txt",header=None,sep='\t')
semeval_val=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\en-val.txt",header=None,sep='\t')
# print(semeval_train[(semeval_train[2]>4)&(semeval_train[2]<5)].head(20))

dframes=[semeval_train,semeval_test,semeval_val]
semeval=pd.concat(dframes)
semeval.columns=['claim1','claim2','similarity']
semeval['match']=semeval[['similarity']].apply(lambda row: 1 if row['similarity']>=2 else 0,axis=1)
del semeval['similarity']
semeval['source']='semeval'
############
ms_training=pd.read_csv(r"C:\\MSRParaphraseCorpus\\msr_paraphrase_train.txt",sep='\t',quoting=csv.QUOTE_NONE)
ms_test=pd.read_csv(r"C:\\MSRParaphraseCorpus\\msr_paraphrase_test.txt",sep='\t',quoting=csv.QUOTE_NONE)
dframes=[ms_training,ms_test]
ms=pd.concat(dframes)
del ms['#1 ID']; del ms['#2 ID']
ms.columns=['match','claim1', 'claim2']
ms['source']='ms'

############

alldata['difficulty']="NA"
save_obj(alldata, "alldata")
# processData(ms_data, df_name="ms_data", embeddings_name="ms_embeddings_matrix")



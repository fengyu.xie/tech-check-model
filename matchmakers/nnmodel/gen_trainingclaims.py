import sys
sys.path.insert(0, '../common')

from utils import PostgreSQLDB
import csv
#Note: run these scripts from VM command line

if __name__=="__main__":

	#connect to VM local copy of db
	db=PostgreSQLDB("techcheck") 
	#Select most recent 1500 entries in STF database which are in English (language_id=1) and from 
	#organizations listed as active fact checkers on the STF website, not including those organizations
	#which publish in another language. If there are duplicate claims, take the claim which is most recent. 
	
	res=db.execute("""SELECT "id", REPLACE(
									REPLACE(
										REPLACE(TRIM("statement"),'"', ''),
									'“',''),
									'”','')
									 AS "statement" FROM
						(SELECT "id", "statement", "date",
						ROW_NUMBER() OVER (PARTITION BY "statement" ORDER BY "date" DESC) AS "row_num"
						FROM facts.facts 
						WHERE "language_id"=1 AND "organization_id" IN (1,2,3,10,14,15,31,37)  
						ORDER BY "date" DESC
						)
						AS "TrainingClaims"
						WHERE "row_num"=1
						LIMIT 1500
						""")

	'''There are still some repeated statements/nonsense test statements in 
		the table generated above, but:
		(1) Repeated statements shouldn't matter because then 2 people would
		reword the statements in a different way. 
		(2) We will add an option to the flask app for somebody to report a claim
			as "This doesn't make sense" to label nonsense claims, and clean it 
			from the data later. 
	'''
	#write data to file
	f=open('/home/vcm/techcheck/claim_matcher/web/trainingclaims.csv', 'w')
	outcsv=csv.writer(f)

	outcsv.writerow(res.keys())
	outcsv.writerows(res)
	# for row in res:
	# 	outcsv.writerow([row.id, row.speaker, row.statement, row.date, row.language_id])
	f.close()
	

	#Experimental code, also some of the code used to get the info in the README
	# row=res.fetchone()
	#for row in res: 
	#	print(row)
		# print(row.items()) #list of tuples
		# print(row["speaker_title"]) #gets specific value in this column

	# tb_names=db.execute("SELECT relname from pg_class where relkind='r' and relname !~ '^(pg_|sql_)';")
	# print(tb_names.fetchall())

	#print schema
	# for table_name in ('categories', 'events', 'organization_languages', 'organizations'):
	# 	print(table_name)
	# 	s="SELECT * FROM facts." + table_name
	# 	tb_id=db.execute(s)
	# 	for _id in tb_id: 
	# 		print (_id)

	#find which organization  is coded 6 (answer: Gossip Cop)
	# org=db.execute('SELECT DISTINCT "id", "name" from facts.organizations ORDER BY "id"')
	# for _org in org: 
	# 	print (_org)
import pandas as pd 
import gensim
import sys
from timeit import default_timer as time
from neuralnetwork_utils import buildDict,load_obj,clean_str,save_obj
root="C:/Users/Caroline Wang/OneDrive/Duke/Data+/techcheck"

# print("reading in csvs")
# mnli_train=pd.read_csv(root+"/interface/carolinesdata/mnli_train.csv")
# mnli_devmatch=pd.read_csv(root+"/interface/carolinesdata/mnli_devmatch.csv")
# mnli_devmismatch=pd.read_csv(root+"/interface/carolinesdata/mnli_devmismatch.csv")
# print("concatenating series")
# concat_series=pd.concat([mnli_train["claim1"],mnli_devmatch["claim1"],mnli_devmismatch["claim1"],
# 						 mnli_train["claim2"],mnli_devmatch["claim2"],mnli_devmismatch["claim2"]])
# print("saving csvs")
# concat_series.to_csv("C:/Users/Caroline Wang/OneDrive/Duke/Data+/techcheck/interface/carolinesdata/mnli_claims.csv")
# sys.exit(0)
###########################
print("loading google model")
google_news_path = root+"/engine/matchmakers/nnmodel/word2vec/data/GoogleNews-vectors-negative300.bin"
google_model = gensim.models.KeyedVectors.load_word2vec_format(google_news_path, binary=True)
# embeddings_masterdict=load_obj(name="embeddings_masterdict",root=root+"/engine/matchmakers/nnmodel/")
# csv=pd.read_csv("C:/Users/Caroline Wang/OneDrive/Duke/Data+/techcheck/interface/carolinesdata/mnli_claims.csv")
# print(csv.shape)
# sys.exit(0)
mnli_embeddings={}
for count,chunk in enumerate(pd.read_csv("C:/Users/Caroline Wang/OneDrive/Duke/Data+/techcheck/interface/carolinesdata/mnli_claims.csv",chunksize=5000)):
	start=time()
	print("chunk no: ", count)
	cleaned_series=chunk.iloc[:,1].apply(clean_str)
	split_series=cleaned_series.str.split()
	mnli_embeddings=buildDict(split_series,google_model,mnli_embeddings)
	print(time()-start)
	save_obj(mnli_embeddings,name="mnli_dict",root=root+"/engine/matchmakers/nnmodel/")


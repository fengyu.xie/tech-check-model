import pickle
import numpy as np
import pandas as pd
import re
import gc
from os import path
from time import time
import sys
basepath=path.dirname(__file__) #path of current script
commonpath=path.abspath(path.join(basepath,"..","..","common"))
sys.path.insert(0, commonpath)
from makerequest import makeGetRequest

def save_obj(obj, name,root=""):
	with open(root+'obj/'+ name + '.pkl', 'wb') as f:
		pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)
	return

def load_obj(name,root=""):
	with open(root+'obj/' + name + '.pkl', 'rb') as f:
		return(pickle.load(f))

def clean_str(string):
	"""
	Tokenization/string cleaning for all datasets except for SST.
	:param string
	:returns: cleaned string 
	Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
	"""
	try: 
		string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
		string = re.sub(r"\'s", " \'s", string)
		string = re.sub(r"\'ve", " \'ve", string)
		string = re.sub(r"n\'t", " n\'t", string)
		string = re.sub(r"\'re", " \'re", string)
		string = re.sub(r"\'d", " \'d", string)
		string = re.sub(r"\'ll", " \'ll", string)
		string = re.sub(r",", " , ", string)
		string = re.sub(r"!", " ! ", string)
		string = re.sub(r"\(", " \( ", string)
		string = re.sub(r"\)", " \) ", string)
		string = re.sub(r"\?", " \? ", string)
		string = re.sub(r"\s{2,}", " ", string)
		return string.lower()
	except TypeError: 
		# print(string, "type: ", type(string))
		return ""

def pad_sentence(sen_list, length, padding_word="<PAD/>"):
	"""
	Pads all sentences to the same word length. The length is defined by the longest sentence.
	:param sen_list: a list words in sentence
	:returns: list of words in sentence, with padding added if necessary and words truncated from end
	if necessary 
	"""
	if len(sen_list)>=length:
		return(sen_list[:length])
	num_padding = length - len(sen_list)
	new_sen_list = sen_list + [padding_word]*num_padding
	return new_sen_list

def process_sen(sentence,length):
	"""
	:param sentence: a sentence 
	:returns: the sentence in list form, of shape (sentence word length, ),
			  cleaned and with all paddings added
	"""
	return pad_sentence(str.split(clean_str(sentence)),length)

def buildDict(pd_series,model,word_vec_dict={}):
	"""
	Encoding V2, where each unique word in a panda series is directly mapped to the word embedding numpy vector
	Maps missing words to 0. 
	:param pd_series: intend to pass in a panda series of all claims in STF database, where elements of series are lists 
	(sentences split on whitespace, padded, and cleaned)
	:param model: intend to pass in google news vecs
	:return: dictionary mapping words to numpy vectors
	"""
	#value_key naming convention for dictionary
	word_vec_dict["<PAD/>"]=np.zeros(300) #associate padding character wtih a len=300 zero vector and the embeddings matrix key 0
	flattened=pd_series.apply(pd.Series).stack()
	unique=flattened.unique()
	num_missing=0

	start=time()
	for word in unique:
		if word in model.vocab:
			word_vec_dict[word]=model[word] #associate a number to each word in dict,leaving out 0 for padding
		else: 
			num_missing+=1
			word_vec_dict[word]=np.zeros(300)
	end=time()

	print("Time to build dictionary of size", len(word_vec_dict),end-start)
	print("Number missing words:", num_missing)

	return(word_vec_dict)

def processData(res,df_name,max_seq_length=127): 
	"""
	Takes pandas df with columns 'claim1', 'claim2' as input, cleans the data and pads to 
	appropriate length, returns a list of words for claim1_encoded and claim2_encoded

	"""
	#pad to 127 because that's the max number words in a sentence in STF 
	#see gen_stf_dict.py for the code to generate that number
	res['claim1_encoded']=res[['claim1']].apply(lambda row: process_sen(row['claim1'],max_seq_length),axis=1)
	res['claim2_encoded']=res[['claim2']].apply(lambda row: process_sen(row['claim2'],max_seq_length),axis=1)

	print("saving dataframe")
	save_obj(res,df_name)
	res.to_csv(df_name+".csv")


def getEmbeddings(ndarray,masterdict,tempdict,ngrok_url="",google_model=None):
	"""
	:param ndarray: a numpy array of any shape, containing words
	:param masterdict: a dictionary mapping all unique words from 
					   STF database to corresponding word vectors
	:param tempdict: a dictionary mapping words to word embedding 
					 vectors, for words not in the masterdict
	:returns: the original numpy array with word embedding vectors 
			  in place of the original words
	"""
	zero_array=np.zeros(ndarray.shape+(300,), dtype=np.float32)
	for i in np.ndindex(ndarray.shape):
		word=str(ndarray[i]) #word is contained in first element of vector
		if word in masterdict:
			zero_array[i]=masterdict[word]
		elif word in tempdict:
			zero_array[i]=tempdict[word]

		elif ngrok_url!="": 
			print("checking api for word ",word)
			#make get request, put returned value in dictionary
			# start=time()
			link=ngrok_url+"/word2vec?word="+word
			json_list=makeGetRequest(link) #api returns list
			# end=time()
			# print("time to make get request: ", end-start)
			if json_list==None :
				#if post request fails, treat word as padding character;
				#i.e.  set it to length 300 zero vector 
			 	zero_array[i]=masterdict["<PAD/>"]
			 	tempdict[word]=zero_array[i]
			 	continue
			vec=np.array(json_list,dtype=np.float32)
			zero_array[i]=vec
			tempdict[word]=vec
		else: 
			# print("checking google model")
			if word in google_model.vocab:
				zero_array[i]=google_model[word] #associate a number to each word in dict,leaving out 0 for padding
			#if word not in google model, it will be associated to 0 in tempdict
			tempdict[word]=zero_array[i]
	return(zero_array)

def to3DArray(X_df,masterdict,tempdict,max_seq_length,root,ngrok_url,google_model): 
	'''
	takes in dataframe with claims as columns, cleans claims, and converts these lists to "wide" columns
	'''
	# print(X_df[['claim1']].head(5))
	X_df['claim1_encoded']=X_df[['claim1']].apply(lambda row: process_sen(row['claim1'],max_seq_length),axis=1)
	X_df['claim2_encoded']=X_df[['claim2']].apply(lambda row: process_sen(row['claim2'],max_seq_length),axis=1)

	X_df1=pd.DataFrame(X_df.claim1_encoded.tolist()).values 
	X_df2=pd.DataFrame(X_df.claim2_encoded.tolist()).values

	stacked_array=np.stack((X_df1,X_df2),axis=2)
	stacked_array=getEmbeddings(stacked_array,masterdict,tempdict,ngrok_url,google_model)
	# save_obj(tempdict, "embeddings_tempdict",root)
	return(stacked_array)


def embeddingsGenerator(csv_path,batch_size,n_epoch,embeddings_masterdict,embeddings_tempdict,max_seq_length,root,ngrok_url,google_model):
	for i in range(n_epoch+1): #so method doesn't raise stopiteration error before epochs are over
		# print("epoch no: ",i)
		gen=pd.read_csv(csv_path,chunksize=batch_size)
		for count, data in enumerate(gen): 
			# print("chunk ", count)
			X_train=to3DArray(X_df=data,masterdict=embeddings_masterdict,tempdict=embeddings_tempdict,
							  max_seq_length=max_seq_length,root=root, ngrok_url=ngrok_url,google_model=google_model)
			Y_train=data['match'].values 
			yield ([X_train[:,:,0], X_train[:,:,1]], Y_train)


# if __name__ == '__main__':
# 	def subGenerator(letters): 
# 		for _ in letters: 
# 			yield _

# 	def testGenerator(num_epochs): 
# 		for i in range(num_epochs): 
# 			gen=subGenerator("abcdefg") 
# 			for count, letter in enumerate(gen): 
# 				yield letter

# 	testgen=testGenerator(5)
# 	while True:
# 		print(next(testgen))



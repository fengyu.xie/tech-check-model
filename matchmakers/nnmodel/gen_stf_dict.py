from neuralnetwork_utils import clean_str,pad_sentence, save_obj, load_obj, build_Dict
import sys; import os
import pandas as pd
import gensim

if __name__ == '__main__':
	#Given a csv containing all the claims of the STF df, generate the unique words, and build a dictionary 
	#mapping words to corresponding word vectors 

	stf_words=pd.read_csv(r"C:\Users\Caroline Wang\OneDrive\Duke\Data+\techcheck\interface\carolinesdata\stf_words.csv")

	#preprocess
	stf_words['statement']=stf_words[['statement']].apply(lambda row: clean_str(row['statement']),axis=1)
	stf_words['statementsplit']=stf_words['statement'].str.split()
	maxwords=stf_words['statementsplit'].str.len().max()
	print("max number words in sentences:",maxwords)
	#Note: had to find out the max number words in sentences to know what length 
	#vector to allow the model to accept. This is completely irrelevant to building  a 
	#dictionary of all unique words to google vecs
	# sys.exit(0)

	stf_words['statementsplit']=stf_words[['statementsplit']].apply(lambda row: pad_sentence(row['statementsplit'],maxwords),axis=1)

	google_news_path = os.path.abspath("GoogleNews-vectors-negative300.bin\\GoogleNews-vectors-negative300.bin")
	google_model = gensim.models.KeyedVectors.load_word2vec_format(google_news_path, binary=True)

	# print(type(google_model['dog'])) #answer: numpy.ndarray
	embeddings_masterdict=buildDict(stf_words['statementsplit'],google_model)
	print("length of dictionary", len(embeddings_masterdict))
	print("data type: ", type(embeddings_masterdict['dog']))
	# save_obj(embeddings_masterdict,"embeddings_masterdict")

from time import time
import datetime
import numpy as np
import pandas as pd
import gc
from operator import itemgetter

from neuralnetwork_utils import process_sen, save_obj,getEmbeddings, to3DArray, embeddingsGenerator
from customlayers import DistanceMerge
print("importing matplotlib")
import matplotlib.pyplot as plt
print("importing keras")
from keras.models import Model as kModel
from keras.models import load_model
from keras.layers import Input, Embedding, LSTM
from keras import backend as K
from keras.optimizers import Adam, SGD
from keras.callbacks import ModelCheckpoint

import sys
from os import path
basepath=path.dirname(__file__) #path of current script
commonpath=path.abspath(path.join(basepath,"..","..","common"))
sys.path.insert(0, commonpath)

from matchmaker import Matchmaker
#from utils import PostgreSQLDB

class NeuralNetwork(Matchmaker):
	def __init__(self, n_hidden, learning_rate, loss_type, metrics, max_seq_length,batch_size,
				n_epoch,embeddings_masterdict, embeddings_tempdict,
				root,load,model_name,custom_objects): #all defaults set in MaLSTM
		super().__init__()
		# self.db=PostgreSQLDB('techcheck')
		# self.dbpos={}
		self.n_hidden=n_hidden
		self.loss_type=loss_type
		self.max_seq_length=max_seq_length
		self.embeddings_masterdict=embeddings_masterdict
		self.embeddings_tempdict=embeddings_tempdict #check dicts in embeddings layer custom
		self.metrics=metrics
		self.optimizer=Adam(learning_rate)
		self.batch_size=batch_size
		self.n_epoch=n_epoch
		self.class_weight={0:1.0, 1:1.0}
		self.model=None
		self.history_callback=None #history object returned by model training
		self.model_name=model_name
		self.custom_objects=custom_objects
		self.root=root
		self.ngrok_url=""
		if load==True: 
			self.model=load_model(self.root+"saved_models/"+model_name+".h5",custom_objects=custom_objects)
			#calling the KERAS function, not mine
			self.model._make_predict_function() #bug in tf, soln is to explicitly first call this fcn

	def compile(self):
		print("compiling model")
		self.model.compile(loss=self.loss_type,optimizer=self.optimizer,metrics=self.metrics)
		return None

	def checkpointModel(self):
		filepath=self.root+"saved_models/"+self.model_name+".bestweights.h5"
		checkpoint=ModelCheckpoint(filepath, monitor='val_acc',verbose=1, save_best_only=True, mode="max")
		return [checkpoint] #callbacks_list

	def plotModel(self): 
		# Plot accuracy
		print("plotting model")
		plt.plot(self.history_callback.history['acc'])
		plt.plot(self.history_callback.history['val_acc'])
		plt.title('Model Accuracy')
		plt.ylabel('Accuracy')
		plt.xlabel('Epoch')
		plt.legend(['Train', 'Validation'], loc='upper left')
		#plt.show(block=False)
		plt.savefig(self.root+"saved_models/"+self.model_name+"accplot")
		plt.close()

		# Plot loss
		plt.plot(self.history_callback.history['loss'])
		plt.plot(self.history_callback.history['val_loss'])
		plt.title('Model Loss')
		plt.ylabel('Loss')
		plt.xlabel('Epoch')
		plt.legend(['Train', 'Validation'], loc='upper right')
		#plt.show(block=False)
		plt.savefig(self.root+"saved_models/"+self.model_name+"accplot")
		plt.close()

	def setClassWeight(self, dataframe):
		num_matches=dataframe["match"].sum()
		self.class_weight={0:num_matches/(dataframe.shape[0]-num_matches), 1:1.0}
		print("percent matches for data: ", num_matches/dataframe.shape[0])

	def trainModel(self,train_data=None, csv_path=None,generate=False,validate_percent=0.0,validation_df=None,checkpoint=False,load_checkpoint="",google_model=None):
		self.initModel()
		self.compile()
		# print(self.model.count_params()) #70,200
		if load_checkpoint!="": 
			print("loading weights from file")
			self.model.load_weights(self.root+"saved_models/"+self.model_name+".bestweights.h5")
		
		callbacks_list=None
		if checkpoint==True: 
			print("best version of this model will be saved")
			callbacks_list=self.checkpointModel()

		print("callbacks: ",callbacks_list)
		print("processing data, starting fitting")
		
		if csv_path!=None and generate==True:
			#csv path,validation data MUST have something for generator to work
			#get class weights
			data=pd.read_csv(csv_path)
			steps_per_epoch=int(data.shape[0]/self.batch_size) #total number of batches before declaring epoch over,must be int
			self.setClassWeight(data)
			del data
			#process validation pd df
			X_val=to3DArray(X_df=validation_df,masterdict=self.embeddings_masterdict,tempdict=self.embeddings_tempdict,
					max_seq_length=self.max_seq_length,root=self.root, ngrok_url=self.ngrok_url,google_model=google_model)
			Y_val=validation_df['match'].values 
			validation_data=([X_val[:,:,0], X_val[:,:,1]], Y_val)
			# del X_val, Y_val

			training_start_time=time()
			self.history_callback=self.model.fit_generator(generator=embeddingsGenerator(csv_path=csv_path,batch_size=self.batch_size,
																						 n_epoch=self.n_epoch,
																						 embeddings_masterdict=self.embeddings_masterdict,
																						 embeddings_tempdict=self.embeddings_tempdict,
																						 max_seq_length=self.max_seq_length,root=self.root,
																						 ngrok_url=self.ngrok_url,google_model=google_model),
								  steps_per_epoch=steps_per_epoch, epochs=self.n_epoch, verbose=1,callbacks=callbacks_list, 
								  validation_data=validation_data,class_weight=self.class_weight,shuffle=True,
								  workers=4,use_multiprocessing=False)
			gc.collect()
			self.plotModel()
			
		else: #generator=False
			if train_data!=None: 	
				train=pd.DataFrame(train_data, columns=['difficulty','claim1','claim2','match'])
			if csv_path!=None:
				train=pd.read_csv(csv_path)
			setClassWeight(train)	

			X_train=to3DArray(X_df=train,masterdict=self.embeddings_masterdict,tempdict=self.embeddings_tempdict,
							  max_seq_length=self.max_seq_length,root=self.root, ngrok_url=self.ngrok_url,google_model=google_model)
			Y_train=train['match'].values 
		
			training_start_time=time()
			self.history_callback=self.model.fit([X_train[:,:,0], X_train[:,:,1]], Y_train, batch_size=self.batch_size, 
								  epochs=self.n_epoch, verbose=1,callbacks=callbacks_list, validation_split=validate_percent,
								  class_weight=self.class_weight,shuffle=True)
		
		print("Training time finished.\n{} epochs in {}".format(self.n_epoch, datetime.timedelta(seconds=time()-training_start_time)))
		print("model history_callback: ", "/naccuracy: ",self.history_callback.history['acc'],"/nvalidation accuracy: ",self.history_callback.history['val_acc'],
		"/nloss: ",self.history_callback.history['loss'],"/nvalidation loss: ",self.history_callback.history['val_loss'])
		self.model._make_predict_function() #bug in tf, soln is to explicitly first call this fcn
		return None
	
	
	def evaluate(self, X_eval, Y_eval, eval_batchsize):
		"""
		Evaluate model performance on a set of labelled data
		"""
		print(self.model.evaluate(x=[X_eval[:,:,0], X_eval[:,:,1]], y=Y_eval, batch_size=eval_batchsize, verbose=1, sample_weight=None, steps=None))

	def saveModel(self,model_name):
		self.model.save(self.root+"saved_models/"+self.model_name+".h5")

	def loadModelFromSaved(self, saved_model_name):
		"""
		If a NeuralNetwork object has already been instantiated, but you want to load another (saved) 
		model into this object
		"""
		self.model=load_model(self.root+"saved_models/"+saved_model_name+".h5",custom_objects=self.custom_objects)

	def predictModel(self, input_array): 
		"""
		This function takes in 2 processed input sentences as a single numpy array, splits
		up the left/right inputs, puts them into a list, and feeds to model. 
		:param input_data: a numpy array with shape (1,self.max_seq_length,2,300)
		:returns a tuple of numpy nd arrays with the model "confidence" and class prediction
		"""
		assert input_array.shape==(1,self.max_seq_length,2,300)
		left_input=input_array[:,:,0]
		right_input=input_array[:,:,1]

		y_prob = self.model.predict([left_input,right_input]) 
		# print(y_prob)
		# y_classes = round(y_prob)
		return y_prob #type: nd arrays

	def compareTwo(self,s1,s2):
		"""
		:param s1: a string
		:param s2: a string
		:returns: a tuple in the format (sentence1, sentence 2, confidence score, match=0 or 1)
		""" 
		if self.model==None: 
			print("Model cannot be None.")
			raise TypeError
		s1_processed=np.expand_dims(np.array(process_sen(s1,self.max_seq_length)),axis=0)
		s2_processed=np.expand_dims(np.array(process_sen(s2,self.max_seq_length)),axis=0)
		assert s1_processed.shape==(1,self.max_seq_length)
		stacked_sen=np.stack((s1_processed,s2_processed),axis=2)
		assert stacked_sen.shape==(1,self.max_seq_length,2)

		stacked_embedded=getEmbeddings(stacked_sen,self.embeddings_masterdict,self.embeddings_tempdict,self.ngrok_url)
		assert stacked_embedded.shape==(1,self.max_seq_length,2,300)

		prediction=self.predictModel(stacked_embedded)
		probability=float(prediction)
		match=int(round(probability))
		return (s1,s2,probability,match)

	def matchSentence_db(self,sentence): 
		"""
		:param sentence: this is a string sentence to compare against the ShareTheFacts database
		:returns a list of tuples in the format [(sentence, matched_sentence,confidence score),...]
		"""
		if self.model==None: 
			print("Model cannot be None.")
			raise ValueError
		sentence_processed=np.expand_dims(process_sen(sentence,self.max_seq_length),axis=0)

		resultTups=[]
		result=self.db.execute("""SELECT DISTINCT "statement","id" FROM facts.facts WHERE "language_id"=1 AND "organization_id" IN (1,2,3,10,14,15,31,37);""")
		for row in result.fetchall():
			# start=time()
			db_sentence=row[0]
			# print("sen1: ", sentence, "db sen: ", db_sentence)
			db_sentence_processed=np.expand_dims(process_sen(db_sentence,self.max_seq_length),axis=0)
			stacked_sen=np.stack((sentence_processed,db_sentence_processed),axis=2)
			stacked_embedded=getEmbeddings(stacked_sen,self.embeddings_masterdict,self.embeddings_tempdict,self.ngrok_url)
			# print(stacked_embedded)
			probability=float(self.predictModel(stacked_embedded))
			match=round(probability)
			if match==1 and probability>=.75:  
				resultTups.append((sentence,db_sentence,probability,match))
			# end=time()
			# print(end-start)
		return sorted(resultTups, key=itemgetter(2), reverse=True)
		self.saveTempDict()

	def saveTempDict(self): 
		save_obj(self.embeddings_tempdict, name="embeddings_tempdict",root=self.root)

	def onFinish(self):
		self.saveTempDict()

class MaLSTM(NeuralNetwork,Matchmaker): 
	def __init__(self, n_hidden, learning_rate, loss_type, metrics, max_seq_length, 
				embeddings_masterdict, embeddings_tempdict, 
				batch_size,n_epoch, root="",load=False,model_name="lstm_model",custom_objects={"DistanceMerge": DistanceMerge}): 
		super().__init__(n_hidden, learning_rate, loss_type, metrics, max_seq_length, 
						batch_size,n_epoch,embeddings_masterdict, embeddings_tempdict,
						root,load,model_name,custom_objects)

	def initModel(self): 
		print("creating embedding layer")
		left_input=Input(shape=(self.max_seq_length,300),dtype='float32')
		right_input=Input(shape=(self.max_seq_length,300),dtype='float32')

		# print("length of embeddings matrix",len(self.embeddings_matrix))
		# embedding_layer = Embedding(len(self.embeddings_matrix),output_dim=300, weights=[self.embeddings_matrix], input_length=self.max_seq_length, trainable=False) 
		# encoded_left=embedding_layer(left_input)
		# encoded_right=embedding_layer(right_input)

		#siamese lstm branches
		print("creating lstm layers")
		shared_lstm=LSTM(units=self.n_hidden,activation='tanh') #relu
		left_output=shared_lstm(left_input)
		right_output=shared_lstm(right_input)

		#calculate distance
		malstm_distance=DistanceMerge(output_dim=1) ([left_output, right_output])

		# Pack it all up into a model
		self.model = kModel([left_input, right_input], [malstm_distance])
		
		
	

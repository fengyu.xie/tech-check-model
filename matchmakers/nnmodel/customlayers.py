from keras import backend as K
from keras import initializers
from keras import regularizers
from keras import constraints
from keras.engine.base_layer import Layer
from keras.legacy import interfaces

class DistanceMerge(Layer):
	def __init__(self,output_dim, **kwargs):
		self.output_dim = output_dim
		super().__init__(**kwargs)

	def build(self, input_shape):
			super().build(input_shape)  # Be sure to call this at the end

	def exponent_neg_manhattan_distance(self, list_inputs): #exponent_neg_manhattan_distance
		''' Helper function for the similarity estimate of the LSTMs outputs'''
		left=list_inputs[0]
		right=list_inputs[1]
		# print("left: ",type(left))
		# print("right:",type(right))
		return K.exp(-K.sum(K.abs(left-right), axis=1, keepdims=True))

	def call(self, x):
		"""
		:param x: tensorflow tensors, left input and right input
		returns e^(-manhattan distance between 2 input vectors)
		"""
		return self.exponent_neg_manhattan_distance(x)

	def compute_output_shape(self, input_shape):
		"""
		in this case, output shape should be a scalar
		"""
		return (input_shape[0][0], self.output_dim)

	def get_config(self):
		config = {'output_dim': self.output_dim}
		base_config = super().get_config()
		return dict(list(base_config.items()) + list(config.items()))



	

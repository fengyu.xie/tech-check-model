import pandas as pd
import numpy as np
import sys
from os import path
from time  import time
import gensim
from neuralnetwork_utils import load_obj, process_sen, to3DArray
from neuralnetwork import MaLSTM

if __name__ == '__main__':
	pd.options.display.max_columns = 10
	pd.options.display.max_rows = 350
	pd.options.display.width=250
	basepath=path.dirname(__file__) #path of current script

	print("loading google model")
	google_news_path =path.abspath(path.join(basepath,"word2vec/data/GoogleNews-vectors-negative300.bin"))
	google_model = gensim.models.KeyedVectors.load_word2vec_format(google_news_path, binary=True,limit=100000)

	print("loading dictionaries")
	start0=time()
	embeddings_masterdict=load_obj("mnli_dict")
	embeddings_tempdict=load_obj("embeddings_tempdict")
	end0=time()
	print("time to load dicts: ", end0-start0)
	print("length of tempdict: ", len(embeddings_tempdict))
	#NeuralNetwork (base class) instance vars
	n_hidden=5
	learning_rate=.0001
	max_seq_length=50 #Xtrain[0,:,0].shape[0] #(row, column) output:127
	loss_type='binary_crossentropy'#'mean_squared_error'
	metrics=['accuracy']
	batch_size=64
	n_epoch=5

	model_name="mnli_losslow"
	malstm=MaLSTM(n_hidden=n_hidden, learning_rate=learning_rate, loss_type=loss_type, 
				  metrics=metrics, max_seq_length=max_seq_length, 
				  embeddings_masterdict=embeddings_masterdict,embeddings_tempdict=embeddings_tempdict,
				  batch_size=batch_size,n_epoch=n_epoch,model_name=model_name)

	np.random.seed(42)
	data_path =path.abspath(path.join(basepath,"..","..","..","interface/carolinesdata"))
	validation_df=pd.read_csv(path.join(data_path,"mnli_devmatch.csv"))
	malstm.trainModel(csv_path=path.join(data_path,"mnli_train.csv"), generate=True,
					  validation_df=validation_df,checkpoint=False,google_model=google_model)
	print("Model specs:", "/nnumber hidden layers ", malstm.n_hidden, "/nlearning rate ", malstm.learning_rate, "/nmax seq length ",malstm.max_seq_length, "/nloss type ", malstm.loss_type, "/nmetrics ", malstm.metrics, "/nbatch size ", malstm.batch_size, "/nnumber epochs ", malstm.n_epoch)

	######################EVALUATION CODE
	print("evaluating on mnli_devmismatch data")
	mnli_devmismatch=pd.read_csv(path.join(data_path,"mnli_devmismatch.csv"))
	X_eval=to3DArray(X_df=mnli_devmismatch,masterdict=self.embeddings_masterdict,tempdict=self.embeddings_tempdict,
					max_seq_length=self.max_seq_length,root=self.root, ngrok_url=self.ngrok_url,google_model=google_model)
	Y_eval=validation_df['match'].values 
	malstm.evaluate(X_eval, Y_eval, eval_batchsize=batch_size)

	s1="Donald Trump supported the war in Iraq."
	s2="Barack Obama supported the war in Iraq."
	s3="my favorite animals are cats and dogs"
	predictions=[malstm.compareTwo(s1,s1), malstm.compareTwo(s1,s2), malstm.compareTwo(s1,s3)]
	print('\n'.join('{}: {}'.format(*k) for k in enumerate(predictions)))	
	# malstm.saveModel(malstm.model_name)
	malstm.onFinish()
	
	# saved_model="lstm_model"
	# malstm_saved=MaLSTM(n_hidden=n_hidden, learning_rate=learning_rate, loss_type=loss_type, 
	# 			  metrics=metrics, max_seq_length=max_seq_length,
	# 			  embeddings_masterdict=embeddings_masterdict,embeddings_tempdict=embeddings_tempdict,
	# 			  batch_size=batch_size,n_epoch=n_epoch, load=True)
	# s1="Donald Trump supported the war in Iraq. fdsfsd"
	# s2="Barack Obama supported the war in Iraq."
	# s3="my favorite animals are cats and dogs"
	# s4="Says Tammy Baldwin is more worried about the mastermind of 9/11 than supporting CIA director nominee Gina Haspel."
	# s5="In terms of the support that we can."
	# predictions=[malstm_saved.compareTwo(s4,s5)]
	# print('\n'.join('{}: {}'.format(*k) for k in enumerate(predictions)))	
	# # dbpred=[malstm_saved.matchSentence_db(s4)]
	# # print(dbpred)
	# malstm_saved.onFinish()
	# sys.exit(0)

	#initializing new model

	##shape verification code
	# print("x_train shape, expected (467,127,2,300):",X_train.shape) 
	# print("input shape, expected (467,127,300):",X_train[:,:,0].shape)
	# print("x_train 1 entry shape, expected (127,300):",X_train[0,:,0].shape)
	# # print("x_train 1st entry:", X_train[0,:,0])
	# # print("X_eval shape: ", X_eval.shape)
	# print(Y_train.shape)
	# print(Y_test.shape)

	#convert pandas dfs to numpy arrays
	# X_train=to3DArray(train,embeddings_masterdict,embeddings_tempdict)
	# X_test=to3DArray(test,embeddings_masterdict,embeddings_tempdict)
	# Y_train=train['match'].values 
	# Y_test=test['match'].values
	# # X_eval=to3DArray(our_data)
	# Y_eval=our_data['match'].values




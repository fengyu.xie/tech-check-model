from random import random, randint

import sys
from os import path
importpath=path.abspath(path.join(path.dirname(__file__),"..","..","common"))
sys.path.insert(0, importpath)
importpath=path.abspath(path.join(path.dirname(__file__),".."))
sys.path.insert(0, importpath)
from matchmaker import Matchmaker
from match import Match

class FakeModel(Matchmaker):
    def __init__(self):
        Matchmaker.__init__(self)

    def compareTwo(self,a,b):
        return (a,b,random(),randint(0,1))
    
    def matchSentence_db(self, sentence):
        return [Match(sentence,'hello world',random()) for _ in range(randint(0,2))] #return between 0 and 2 matches with random confidence levels

    def trainModel(self, _):
        pass
    
    def onFinish(self):
        pass
import re
import csv
import nltk
from tqdm import tqdm
from math import log
from operator import itemgetter
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
from nltk.corpus import state_union as corpus

class IDFGenerator:
    def __init__(self):
        self.stoplist=[x.strip() for x in open("stoplist.txt").readlines() if len(x)>0]
        self.wn_pos_code={'NN':'n','VB':'v','JJ':'a','RB':'r'} #dictionary to turn NLTK pos tags into wordnet synset pos tags
        self.dict={}
    
    def run(self):
        pbar = tqdm(total=len(corpus.sents()), position=0, ncols=100, mininterval=1.0) #init
        for sent in corpus.sents():
            claim=' '.join(sent).strip() #array of words to sentence
            claim=claim.replace(' \' ','\'') #corpus seperates punction so want ["Obama","'"',"s"]=>Obama ' s=>Obama's
            claim=claim.lower().replace('ąx',' ').replace('ąś',' ').replace('ą',' ') #deal with weird corpus specific problems

            tags=self.getPOSTags(claim) #get word, pos tuples
            tags=list(set(tags)) #only want distinct word, pos pairs
            self.writeToDict(tags) #update frequency dictionary
            pbar.update(1) #update progress

        #write dictionary to csv
        self.outputDict()
    
    def getPOSTags(self, text):
		#filter out non-words
        l=[x.lower() for x in re.sub(r'[^\w\s-]','',text).replace("-"," ").split(" ") if len(x)>0]
        #get POS tags, convert to wordnet pos codes
        return [(word,self.wn_pos_code[nltk_pos[:2]] if nltk_pos[:2] in self.wn_pos_code.keys() else '') for word,nltk_pos in nltk.pos_tag(l)]

    def writeToDict(self,tags):
        for word,pos in tags:
            if pos: #n,v,a,r
                key=pos+word #(body,n) => nbody
                if key not in self.dict:
                    self.dict[key]=0
                self.dict[key]+=1
    
    def outputDict(self):
        total=len(corpus.sents()) #total number of sentences
        with open('idf.csv', "w") as f:
            writer=csv.writer(f, delimiter=',', lineterminator='\n')
            for key,count in self.dict.items():
                try:
                    writer.writerow([key,self.computeIDF(count,total)])
                except:
                    print('cannot write',key,self.computeIDF(count,total))

            #make the default equivalent to appearing 0 times
            writer.writerow(['DEFAULT',self.computeIDF(0,total)])
    
    def computeIDF(seld,count,total):
        return log(total/(count+1)) #adding 1 to count allows us to use count=0 as a default for terms that never appear

IDFGenerator().run()


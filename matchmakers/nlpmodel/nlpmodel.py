import sys
sys.path.insert(0, '/home/vcm/techcheck/common')
sys.path.insert(0, '/home/vcm/techcheck/engine/common')
sys.path.insert(0, '/home/vcm/techcheck/engine/matchmakers')
#0 means check this path for folder/modules before checking Python library
from utils import PostgreSQLDB
import re
from operator import itemgetter
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
import heapq
from math import sqrt
from time import time
import csv
from sklearn import svm
import pickle
from matchmaker import Matchmaker
from match import Match

class NLPModel(Matchmaker):
	
	def __init__(self,load=True):
		super().__init__()
		self.db=PostgreSQLDB('techcheck')
		self.dbpos={}
		
		self.clf=None
		self.cache={}
		self.wordWeights={}
		self._createDatabasePOSTupleDict()
		self._createWordWeightsDict()
		self._loadCache()
		if load:
			self._loadClassifier()
		#self.stoplist=[x.strip() for x in open("stoplist.txt").readlines() if len(x)>0] 
		print("init done")

	def onFinish(self):
		self._pickleCache()

	def matchSentence_db(self, sentence):
		print("Starting sentence")
		if self.clf==None:
			raise ValueError
		matches=[]
		claimFeatures=[]
		live_pos_divided = self._divideRelevantWordsByPOS(sentence)
		result=self.db.execute("""SELECT DISTINCT "statement","id","redirectURL","organization_id","speaker","speaker_title","ratingURL","rating" FROM facts.facts WHERE "language_id"=1 AND "organization_id" IN (1,2,3,10,14,15,31,37) AND "date">='2018-01-29' AND "date"<='2018-02-20' AND "speaker" like 'Donald Trump' AND "publication" like '%State of the Union%';""")
		for row in result.fetchall():
			claim_id = row[1]
			if str(claim_id) in self.dbpos.keys():
				new_pos_divided = self.dbpos[str(claim_id)]
			else:
				new_pos_divided = self._divideRelevantWordsByPOS(row[0],claim_id)
			claimFeatures.append((row[0],self._getPOSSimilarityScores(live_pos_divided,new_pos_divided),[row[1],row[2],row[3],row[4],row[5],row[6],row[7]]))
		for claim,features,other in claimFeatures:
			prediction=self.clf.predict([features])[0]
			if prediction=='1':
				conf=self.clf.decision_function([features])[0]
				matches.append(Match(live_sentence=sentence, db_sentence=claim, confidence=conf, stf_id=other[0], url=other[1], organization_id=other[2], rating=other[6], ratingURL=other[5], speaker=other[3], title=other[4]))
		return sorted(matches, key=lambda x: x.confidence, reverse=True)

	def compareTwo(self, s1, s2, predict=True):
		if predict and self.clf==None:
			raise ValueError
		scores=[0,0,0,0]
		pos_divided1=self._divideRelevantWordsByPOS(s1)
		pos_divided2=self._divideRelevantWordsByPOS(s2)
		for i in range(4):
			pos_list1=pos_divided1[i]
			pos_list2=pos_divided2[i]
			#-------
			#print(pos_list1,pos_list2)
			scores[i]=self._sum_max_similarities_weighted(['n','v','a','r'][i],pos_list1, pos_list2) if len(pos_list1)>0 and len(pos_list2)>0 else 0
		if not predict:
			return scores
		prediction=self.clf.predict([scores])
		return (s1,s2,self.clf.decision_function([scores])[0],self.clf.predict([scores])[0])
		
	def trainModel(self,datapath):
		X,y=[],[]
		data=[]
		if type(datapath) is str:
			with open(datapath) as training:
				reader=csv.reader(training)
				data=[row for row in reader if len(row)>0]
		else:
			data=datapath

		#don't train on the title!
		for c,row in enumerate(data[1:]):
			if c<10:
				print(row[0],row[1],row[2],row[3])
			X+=[self.compareTwo(row[1],row[2],predict=False)]
			y+=row[3]
		print(len(X),len(y))
		print(X[:10],y[:10])
		self.clf=svm.SVC(class_weight="balanced")
		self.clf.fit(X,y)
		print("Model created and saved.")
		self._save_obj(self.clf,"clf")

	def _divideRelevantWordsByPOS(self,text,claim_id=-1):
		'''
		@param claim_pos: list of tuples where the first item in the tuple is a word in the sentence and the second is its WN POS
		@returns tuple of lists, divide by part of speech (n,v,j,r)
		'''
		wn_pos_codes={'NN':'n','VB':'v','JJ':'a','RB':'r'} #dictionary to turn NLTK pos tags into wordnet synset pos tags

		#preprocessing: take out irrelevant characters, get POS tags
		words=[x.lower() for x in re.sub(r'[^\w\s-]','',text).replace("-"," ").split(" ") if len(x)>0]
		words_with_pos=[(word,wn_pos_codes[nltk_pos[:2]] if nltk_pos[:2] in wn_pos_codes.keys() else '') for word,nltk_pos in nltk.pos_tag(words)]

		#divide by part of speech for the four we are collecting 
		nouns, verbs, adjs, adverbs = [],[],[],[]
		for word,pos in words_with_pos:
			if pos=='n':
				nouns.append(word)
			elif pos=='v':
				verbs.append(word)
			elif pos=='a':
				adjs.append(word)
			elif pos=='r':
				adverbs.append(word)
		if claim_id!=-1:
			s=self._save_obj((nouns,verbs,adjs,adverbs),"pos",return_str=True)
			self.db.execute(f'''UPDATE facts.facts SET "serialization" = :serialization WHERE id = :claim_id''',serialization=s, claim_id=claim_id)
		return (nouns, verbs, adjs, adverbs)

	def _getPOSSimilarityScores(self, live_pos_divided, new_pos_divided):
		scores=[0,0,0,0]
		for i in range(len(new_pos_divided)):
			pos_list1=new_pos_divided[i]
			pos_list2=live_pos_divided[i]
			scores[i]=self._sum_max_similarities_weighted(['n','v','a','r'][i],pos_list1, pos_list2) if len(pos_list1)>0 and len(pos_list2)>0 else 0
		return scores

	def _sum_max_similarities_weighted(self, pos_type, pos_list1, pos_list2):
		total=0
		totalweight=0
		shorter,longer=(pos_list1,pos_list2) if len(pos_list1)<len(pos_list2) else (pos_list2,pos_list1)
		for word in shorter:
			max_simscore=0
			max_word=""
			for comparison in longer:
				if word==comparison:
					max_simscore=1
					max_word=comparison
					break
				key="+".join(sorted([word,comparison,pos_type])) #way to uniquely identify two-word pairs 
				if key not in self.cache.keys():
					try:
						#check if the word has a synset for its P.O.S. tag by trying to create one
						s1=wordnet.synset(f'{word}.{pos_type}.01')
						s2=wordnet.synset(f'{comparison}.{pos_type}.01')
						sim=s1.wup_similarity(s2)
						self.cache[key] = sim if sim is not None else 0
					except:
						#word doesn't have synset (uncommon word, speech-to-text error, proper noun, etc)
						self.cache[key] = 0
				if self.cache[key]>max_simscore:
					max_simscore=self.cache[key]
					max_word=comparison
			weight1=float(self.wordWeights[f'{pos_type}{word}'] if pos_type+word in self.wordWeights.keys() else self.wordWeights['DEFAULT'])
			weight2=float(self.wordWeights[f'{pos_type}{max_word}'] if pos_type+max_word in self.wordWeights.keys() else self.wordWeights['DEFAULT'])
			total+=max_simscore*weight1*weight2
			totalweight+=(weight1*weight2)
			#------
			#(max_simscore,max_word,weight1,weight2)
		return self._weightHarmonicMean(len(pos_list1),len(pos_list2),total/totalweight)

	def _weightHarmonicMean(self,a,b,total):
		return ((a+b)*total)/(2*b)

	def _pickleCache(self):
		self._save_obj(self.cache,"cache")
		print("Pickled cache")

	def _loadClassifier(self):
		#load the classifier, if one exists
		try:
			self.clf=self._load_obj("clf")
			print("Existing model found. Reading it in...")
		except:
			print("No model found. Run trainModel to create one. ")

	def _loadCache(self):
		#load the cache, if one exists
		try:
			self.cache=self._load_obj("cache")
			print("Cache found. Reading it in...")
		except:
			print("No cache found.")

	def _createWordWeightsDict(self):
		with open('/home/vcm/techcheck/engine/matchmakers/nlpmodel/idf.csv') as idf:
			reader=csv.reader(idf)
			for (word_with_pos,score) in reader:
				self.wordWeights[word_with_pos]=score

	def _createDatabasePOSTupleDict(self):
		#load the already-analyzed POS tuples from the database
		result=self.db.execute('''SELECT "serialization","id" FROM facts.facts WHERE serialization IS NOT NULL''')
		for row in result.fetchall():
			self.dbpos[str(row[1])]=pickle.loads(row[0])

	def _load_obj(self, name):
		with open('/home/vcm/techcheck/engine/matchmakers/nlpmodel/obj/' + name + '.pkl', 'rb') as f:
			return(pickle.load(f))

	def _save_obj(self, obj, name, return_str=False):
		with open('/home/vcm/techcheck/engine/matchmakers/nlpmodel/obj/'+ name + '.pkl', 'wb') as f:
			if return_str:
				return pickle.dumps(obj, pickle.HIGHEST_PROTOCOL)
			else:
				return pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

if __name__=="__main__":
	pass
	'''
	cm=ClaimMatcherEngine()
	#cm.retrainCLF()
	cm.generate_features("Since the election, we have created 2.4 million new jobs","There have been about 1.8 million jobs created since January 2017, according to the Bureau of Labor Statistics")
	db2=PostgreSQLDB('techcheck')
	# result=db2.execute("""SELECT "claim_to_compare","statement" FROM facts.training INNER JOIN facts.facts ON facts.facts.id=facts.training.id ORDER BY random() LIMIT 10;""")
	#start=time()
	with open("sotu.txt","r") as f:
		sentences=f.readlines()[0].split(".")
		for sentence in sentences:
			claimsFeatures=cm.getDBClaimsFeatures(text=sentence)
			#[(claim, features), (claim,...)]
			for claim,features in claimsFeatures:
				prediction=cm.clf.predict([features])[0]
				if prediction=='1':
					conf=cm.clf.decision_function([features])[0]
					if float(conf)>0.7:
						print(f"------------\nOBAMA SAID: {sentence}\nMATCHED WITH: {claim}\nCONFIDENCE: {round(float(conf),2)}\n------------")
						#with open('training.csv', 'a') as f:
						#	writer = csv.writer(f)
						#	yn=input("Good claim? (y/n): ")
						#	if yn[:1].lower()=='y':
						#		writer.writerow(["0",sentence,claim,"1"])
						#	elif yn[:1].lower()=='n':
						#		writer.writerow(["0",sentence,claim,"0"])	

		# s11=testobj.getSimilarClaims(3)[1][0].replace('"', '').replace('“','').replace('”','').strip()
		# s12=testobj.getSimilarClaims(3)[2][0].replace('"', '').replace('“','').replace('”','').strip()
		# s2=pair[1].replace('"', '').replace('“','').replace('”','').strip()
		# if nltk.edit_distance(s1,s2)<=3:
		# 	print("[got same]")
		# else:
		# 	print("%s,%s,%s LOOKING FOR %s" % (s1,s11,s12,s2))
	cm.pickleCache()
	print("done.")
	'''
	


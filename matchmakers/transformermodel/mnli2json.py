import pandas as pd 
import sys
import csv
from os import path
import jsonlines

def label_encoder(string): 
	if string=="neutral": 
		return 0
	if string=="contradiction": 
		return 1 
	if string=="entailment": 
		return 2
	if string=="hidden":
		return -1

	
def write2file(string,filename):
	textfile = open(filename, 'w')
	textfile.write(string)
	textfile.close()


def json2jsonl(writefile,readfile): 
	with jsonlines.open(writefile, mode='w') as writer: 
		with jsonlines.open(readfile) as reader:
			for obj in reader:
				if obj['gold_label'] is not '-': #ignore examples on which annotators don't agree
					obj["label"]=label_encoder(obj["gold_label"])
					obj['bin_label']=obj['label'] if obj['label']<=0 else 1 
					delkeys=[]
					for key, _ in obj.items():
						if key not in ["sentence1","sentence2", "label", "bin_label"]: 
							delkeys.append(key)
					for key in delkeys: 
						del obj[key]
					writer.write(obj)

	
if __name__ == '__main__':
	#code to convert the jsonl utf-8 encoded files to utf-8-sig
	with open('mnli_dev_feats.jsonl','r',encoding='utf-8-sig') as f:
		print(f.read())

	# f=open('mnli_dev_feats.jsonl',encoding="utf-8-").read().decode('utf-8-sig')
	# write2file(f,"temp_mnli_dev_feats.jsonl")
	# f=open('mnli_test_feats.jsonl').read().decode('utf-8-sig')
	# write2file(f,"temp_mnli_test_feats.jsonl")
	# f=open('mnli_train_feats.jsonl').read().decode('utf-8-sig')
	# write2file(f,"temp_mnli_train_feats.jsonl")

	# json2jsonl(writefile="out.jsonl",readfile="in.jsonl")
	sys.exit(0)

	#at some point had code to convert the given mnli json files to jsonl
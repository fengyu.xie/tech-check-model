import sys
import os
import time
import math
import json
import joblib
import random
import argparse
import numpy as np
import tensorflow as tf
from datetime import timedelta

from tqdm import tqdm
from functools import partial
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score

from opt import adam, warmup_cosine, warmup_linear, warmup_constant
from datasets import mnli
from analysis import mnli  as mnli_analysis
from text_utils import TextEncoder
from utils import encode_dataset, flatten, iter_data, find_trainable_variables, get_ema_vars, convert_gradient_to_tensor, shape_list, ResultLogger, assign_to_gpu, average_grads, make_path

############ BEGIN NN FUNCS ############ 
def gelu(x):
	return 0.5*x*(1+tf.tanh(math.sqrt(2/math.pi)*(x+0.044715*tf.pow(x, 3))))

def swish(x):
	return x*tf.nn.sigmoid(x)

opt_fns = {
	'adam':adam,
}

act_fns = {
	'relu':tf.nn.relu,
	'swish':swish,
	'gelu':gelu
}

lr_schedules = {
	'warmup_cosine':warmup_cosine,
	'warmup_linear':warmup_linear,
	'warmup_constant':warmup_constant,
}

def _norm(x, g=None, b=None, e=1e-5, axis=[1]):
	u = tf.reduce_mean(x, axis=axis, keepdims=True)
	s = tf.reduce_mean(tf.square(x-u), axis=axis, keepdims=True)
	x = (x - u) * tf.rsqrt(s + e)
	if g is not None and b is not None:
		x = x*g + b
	return x

def norm(x, scope, axis=[-1]):
	with tf.variable_scope(scope):
		n_state = shape_list(x)[-1]
		g = tf.get_variable("g", [n_state], initializer=tf.constant_initializer(1))
		b = tf.get_variable("b", [n_state], initializer=tf.constant_initializer(0))
		g, b = get_ema_vars(g, b)
		return _norm(x, g, b, axis=axis)

def dropout(x, pdrop, train):
	if train and pdrop > 0:
		x = tf.nn.dropout(x, 1-pdrop)
	return x

def mask_attn_weights(w):
	n = shape_list(w)[-1]
	b = tf.matrix_band_part(tf.ones([n, n]), -1, 0)
	b = tf.reshape(b, [1, 1, n, n])
	w = w*b + -1e9*(1-b)
	return w

def _attn(q, k, v, train=False, scale=False):
	w = tf.matmul(q, k)

	if scale:
		n_state = shape_list(v)[-1]
		w = w*tf.rsqrt(tf.cast(n_state, tf.float32))

	w = mask_attn_weights(w)
	w = tf.nn.softmax(w)

	w = dropout(w, attn_pdrop, train)

	a = tf.matmul(w, v)
	return a

def split_states(x, n):
	x_shape = shape_list(x)
	m = x_shape[-1]
	new_x_shape = x_shape[:-1]+[n, m//n]
	return tf.reshape(x, new_x_shape)

def merge_states(x):
	x_shape = shape_list(x)
	new_x_shape = x_shape[:-2]+[np.prod(x_shape[-2:])]
	return tf.reshape(x, new_x_shape)

def split_heads(x, n, k=False):
	if k:
		return tf.transpose(split_states(x, n), [0, 2, 3, 1])
	else:
		return tf.transpose(split_states(x, n), [0, 2, 1, 3])

def merge_heads(x):
	return merge_states(tf.transpose(x, [0, 2, 1, 3]))

def conv1d(x, scope, nf, rf, w_init=tf.random_normal_initializer(stddev=0.02), b_init=tf.constant_initializer(0), pad='VALID', train=False):
	with tf.variable_scope(scope):
		nx = shape_list(x)[-1]
		w = tf.get_variable("w", [rf, nx, nf], initializer=w_init)
		b = tf.get_variable("b", [nf], initializer=b_init)
		if rf == 1: #faster 1x1 conv
			c = tf.reshape(tf.matmul(tf.reshape(x, [-1, nx]), tf.reshape(w, [-1, nf]))+b, shape_list(x)[:-1]+[nf])
		else: #was used to train LM
			c = tf.nn.conv1d(x, w, stride=1, padding=pad)+b
		return c

def attn(x, scope, n_state, n_head, train=False, scale=False):
	assert n_state%n_head==0
	with tf.variable_scope(scope):
		c = conv1d(x, 'c_attn', n_state*3, 1, train=train)
		q, k, v = tf.split(c, 3, 2)
		q = split_heads(q, n_head)
		k = split_heads(k, n_head, k=True)
		v = split_heads(v, n_head)
		a = _attn(q, k, v, train=train, scale=scale)
		a = merge_heads(a)
		a = conv1d(a, 'c_proj', n_state, 1, train=train)
		a = dropout(a, resid_pdrop, train)
		return a

def mlp(x, scope, n_state, train=False):
	with tf.variable_scope(scope):
		nx = shape_list(x)[-1]
		act = act_fns[afn]
		h = act(conv1d(x, 'c_fc', n_state, 1, train=train))
		h2 = conv1d(h, 'c_proj', nx, 1, train=train)
		h2 = dropout(h2, resid_pdrop, train)
		return h2

def block(x, scope, train=False, scale=False):
	with tf.variable_scope(scope):
		nx = shape_list(x)[-1]
		a = attn(x, 'attn', nx, n_head, train=train, scale=scale)
		n = norm(x+a, 'ln_1')
		m = mlp(n, 'mlp', nx*4, train=train)
		h = norm(n+m, 'ln_2')
		return h

def embed(X, we):
	we = convert_gradient_to_tensor(we)
	e = tf.gather(we, X)
	h = tf.reduce_sum(e, 2)
	return h


def clf_mnli(x, w_init=tf.random_normal_initializer(stddev=0.02), b_init=tf.constant_initializer(0), train=False, ordinal=False):
	out_sz = 3 if ordinal else 2 #out size is number of categories, if in ordinal mode this is 3 (entailment,contradiction,neutral), otherwise 2 (match, non-match)
	#final linear layer, for single input
	#name: clf for classify
	with tf.variable_scope('clf_mnli'):
		nx = shape_list(x)[-1]
		w = tf.get_variable('w', [nx, out_sz], initializer=w_init)
		b = tf.get_variable('b', [out_sz], initializer=b_init)
		return tf.matmul(x, w)+b

#MAIN MODELS
def model_mnli(X, M, Y, train=False, reuse=False, ordinal=False):
	"""
		X: [batch, n_ctx, 2]
		M: [batch, n_ctx]
		Y: [batch]
	"""
	with tf.variable_scope('model', reuse=reuse):
		we = tf.get_variable("we", [n_vocab+n_special+n_ctx, n_embd], initializer=tf.random_normal_initializer(stddev=0.02))
		we = dropout(we, embd_pdrop, train)

		#transformer blocks
		h = embed(X, we)
		print("n_layer: ",n_layer)

		for layer in range(n_layer):
			h = block(h, 'h%d'%layer, train=train, scale=True)

		#language modeling objective function, i.e. the optimization fcn
		lm_h = tf.reshape(h[:, :-1], [-1, n_embd])
		lm_logits = tf.matmul(lm_h, we, transpose_b=True)
		lm_losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=lm_logits, labels=tf.reshape(X[:, 1:, 0], [-1]))
		lm_losses = tf.reshape(lm_losses, [shape_list(X)[0], shape_list(X)[1]-1])
		lm_losses = tf.reduce_sum(lm_losses*M[:, 1:], 1)/tf.reduce_sum(M[:, 1:], 1)

		clf_h = tf.reshape(h, [-1, n_embd])
		#get length of each example
		pool_idx = tf.cast(tf.argmax(tf.cast(tf.equal(X[:, :, 0], clf_token), tf.float32), 1), tf.int32)
		#just takes the state of the transformer at the end of the input, I think?
		clf_h = tf.gather(clf_h, tf.range(shape_list(X)[0], dtype=tf.int32)*n_ctx+pool_idx)

		#reshape to [batch, embed size]
		clf_h = tf.reshape(clf_h, [-1, 1, n_embd])
		if train and clf_pdrop > 0:
			shape = shape_list(clf_h)
			shape[1] = 1
			clf_h = tf.nn.dropout(clf_h, 1-clf_pdrop, shape)
		#put tensor back into (batch, embed size) shape
		clf_h = tf.reshape(clf_h, [-1, n_embd])
		#linear layer
		clf_logits = clf_mnli(clf_h, train=train, ordinal=ordinal)

		#final softmax
		clf_losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=clf_logits, labels=Y)

		return clf_logits, clf_losses, lm_losses


def mgpu_train(*xs, **kwargs): #mgpu for multiple gpu
	"""
	See the argparse in main for the kwargs. 
	This function initializes the computational graph for training. 
	It splits the training data (xs) by number of gpus, assigns the data 
	to a gpu. Each gpu tower calls the training method. At the end, the gradients from 
	the gpus are averaged and used to update weights.Returns the training function. 
	"""
	gpu_ops = []
	gpu_grads = []
	xs = (tf.split(x, n_gpu, 0) for x in xs)
	for i, xs in enumerate(zip(*xs)):
		do_reuse = True if i > 0 else None #reuse var after var are initialize
		with tf.device(assign_to_gpu(i, "/gpu:0")), tf.variable_scope(tf.get_variable_scope(), reuse=do_reuse):
			if dataset == 'mnli': #currently only dataset format this code can train on is mnli
				clf_logits, clf_losses, lm_losses = model_mnli(*xs, train=True, reuse=do_reuse, ordinal=ordinal)
			if lm_coef > 0:
				train_loss = tf.reduce_mean(clf_losses) + lm_coef*tf.reduce_mean(lm_losses)
			else:
				train_loss = tf.reduce_mean(clf_losses)
			params = find_trainable_variables("model")
			grads = tf.gradients(train_loss, params)
			grads = list(zip(grads, params))
			gpu_grads.append(grads)
			gpu_ops.append([clf_logits, clf_losses, lm_losses])
	ops = [tf.concat(op, 0) for op in zip(*gpu_ops)]
	grads = average_grads(gpu_grads)
	grads = [g for g, p in grads]
	train = opt_fns[opt](params, grads, lr, partial(lr_schedules[lr_schedule], warmup=lr_warmup), n_updates_total, l2=l2, max_grad_norm=max_grad_norm, vector_l2=vector_l2, b1=b1, b2=b2, e=e)

	return [train]+ops

def mgpu_predict(*xs, **kwargs):
	"""
	This function enables prediction using multiple gpus
	"""
	gpu_ops = []
	xs = (tf.split(x, n_gpu, 0) for x in xs)
	for i, xs in enumerate(zip(*xs)):
		with tf.device(assign_to_gpu(i, "/gpu:0")), tf.variable_scope(tf.get_variable_scope(), reuse=True):
			if dataset == 'mnli':
				clf_logits, clf_losses, lm_losses = model_mnli(*xs, train=False, reuse=True, ordinal=ordinal)
			gpu_ops.append([clf_logits, clf_losses, lm_losses])
	ops = [tf.concat(op, 0) for op in zip(*gpu_ops)]
	return ops
############ END NN FUNCS ############ 


def transform_mnli(X1, X2):
	"""
	Glue claims together with delimiter and stuff, and add position tokens
	:returns tuple of numpy arrays
	"""
	n_batch = len(X1)
	xmb = np.zeros((n_batch, n_ctx, 2), dtype=np.int32)
	mmb = np.zeros((n_batch, n_ctx), dtype=np.float32)
	start = encoder['_start_']
	delimiter = encoder['_delimiter_']
	for i, (x1, x2) in enumerate(zip(X1, X2)):
		#concatenate
		x = [start] + x1[:max_len]+[delimiter]+x2[:max_len]+[clf_token]
		l = len(x)
		#set np array
		xmb[i,:l,0] = x
		#mask
		mmb[i,:l] = 1
	#position tokens
	xmb[:,:,1] = np.arange(n_vocab+n_special, n_vocab+n_special+n_ctx)
	return xmb, mmb

def iter_apply(Xs, Ms, Ys):
	fns = [lambda x:np.concatenate(x, 0), lambda x:float(np.sum(x))]
	results = []
	for xmb, mmb, ymb in iter_data(Xs, Ms, Ys, n_batch=n_batch_train, truncate=False, verbose=True):
		n = len(xmb)
		if n == n_batch_train:
			res = sess.run([eval_mgpu_logits, eval_mgpu_clf_loss], {X_train:xmb, M_train:mmb, Y_train:ymb})
		else:
			res = sess.run([eval_logits, eval_clf_loss], {X:xmb, M:mmb, Y:ymb})
		res = [r*n for r in res]
		results.append(res)
	results = zip(*results)
	return [fn(res) for res, fn in zip(results, fns)]

def iter_predict(Xs, Ms):
	"""
	Iterates through Xs and Ms and makes predictions
	:param Xs: any iterable with bracket based indexing
	:param Ms: same as above
	:returns a np array of prediction results
	"""
	logits = []
	for xmb, mmb in iter_data(Xs, Ms, n_batch=n_batch_train, truncate=False, verbose=True):
		n = len(xmb)
		if n == n_batch_train:
			logits.append(sess.run(eval_mgpu_logits, {X_train:xmb, M_train:mmb}))
		else:
			logits.append(sess.run(eval_logits, {X:xmb, M:mmb}))
	logits = np.concatenate(logits, 0)
	return logits

def save(path):
	ps = sess.run(params)
	joblib.dump(ps, make_path(path))

def log():
	global best_score
	tr_logits, tr_cost = iter_apply(trX[:n_valid], trM[:n_valid], trY[:n_valid])
	va_logits, va_cost = iter_apply(vaX, vaM, vaY)
	tr_cost = tr_cost/len(trY[:n_valid])
	va_cost = va_cost/n_valid
	tr_acc = accuracy_score(trY[:n_valid], np.argmax(tr_logits, 1))*100.
	va_acc = accuracy_score(vaY, np.argmax(va_logits, 1))*100.
	logger.log(n_epochs=n_epochs, n_updates=n_updates, tr_cost=tr_cost, va_cost=va_cost, tr_acc=tr_acc, va_acc=va_acc)
	print('%d %d %.3f %.3f %.2f %.2f'%(n_epochs, n_updates, tr_cost, va_cost, tr_acc, va_acc))
	if submit:
		score = va_acc
		if score > best_score:
			best_score = score
			save(os.path.join(save_dir, desc, 'best_params.jl'))

argmax = lambda x:np.argmax(x, 1)

pred_fns = {
	'mnli': argmax
}

filenames = {
	'mnli': 'mnli_preds.tsv'
}

label_decoders = {
	'mnli': None
}

analyses = {
	'mnli': mnli_analysis
 }

def get_analysis_func(): 
	return analyses

def get_filenames(): 
	return filenames

def get_pred_func():
	return pred_fns

def predict(test):
	"""
	Writes predictions to the submission directory
	"""
	filename = filenames[dataset]
	pred_fn = pred_fns[dataset]
	label_decoder = label_decoders[dataset]
	if test:
		predictions = pred_fn(iter_predict(teX, teM))
	else:
		predictions = pred_fn(iter_predict(vaX, vaM))
	if label_decoder is not None:
		predictions = [label_decoder[prediction] for prediction in predictions]
	path = os.path.join(submission_dir, filename)
	os.makedirs(os.path.dirname(path), exist_ok=True)
	with open(path, 'w') as f:
		f.write('{}\t{}\n'.format('index', 'prediction'))
		for i, prediction in enumerate(predictions):
			f.write('{}\t{}\n'.format(i, prediction))

def get_args(**kwargs): 
	"""
	Returns a dictionary of all the arguments
	"""
	args={'desc': 'fillindescription', #name for the run
		  'dataset': 'mnli',
		  'log_dir': 'log/',
		  'save_dir': 'save/',
		  'data_dir': '../',
		  'submission_dir': 'submission/',
		  'submit': True, #flag to get test predictions
		  'analysis': True, #flag to run analysis
		  'ordinal': True, #if flag set to true, will get 3 class prediction instead of binary
		  'test': True, #flag to run on test
		  'seed': 42, #random seed
		  'n_iter': 3, #epochs
		  'n_batch': 8, #number in batch per gpu
		  'max_grad_norm': 1,
		  'lr': 6.25e-5,
		  'lr_warmup':.002 ,
		  'n_ctx': 512, #if longest input is longer than this, crop it to this size
		  'n_embd':768, #embedding size
		  'n_head': 12, #attention heads
		  'n_layer': 12, #transformer blocks
		  'embd_pdrop': 0.1,
		  'attn_pdrop': 0.1,
		  'resid_pdrop': 0.1,
		  'clf_pdrop': 0.1,
		  'l2': 0.01,
		  'vector_l2': True, 
		  'n_gpu': 1,
		  'opt': 'adam', #optimization algorithm
		  'afn':'gelu', #activation fcn
		  'lr_schedule':'warmup_linear',
		  'encoder_path':'model/encoder_bpe_40000.json',
		  'bpe_path':'model/vocab_40000.bpe',
		  'encoder_path':'model/encoder_bpe_40000.json',
		  'bpe_path':'model/vocab_40000.bpe',
		  'n_transfer':12, #transformer blocks to keep fixed (default to all)
		  'lm_coef':0.5,
		  'b1':0.9,
		  'b2':0.999,
		  'e':1e-8,
		  }
	
	#overwrite args with passed args where applicable
	for k,v in kwargs.items():
		args[k]=v
	return args

if __name__ == '__main__':
	print("xkcd entering main of transformer.py")

	startTime=timer()
	args = get_args() #return type dict
	print(args)

	globals().update(args)
	random.seed(seed)
	np.random.seed(seed)
	tf.set_random_seed(seed)

	print("xkcd set up log file")
	log_file = os.path.join(log_dir, '{}.jsonl'.format(desc))
	logger = ResultLogger(path=log_file, **args) #set up logging and log parser parameters

	# formatting stuf
	print("xkcd encoding begun")
	text_encoder = TextEncoder(encoder_path, bpe_path) #set up the encoder 
	encoder = text_encoder.encoder	#store encoder (dictionary from token to number) (unclear where numbers come from)
	n_vocab = len(text_encoder.encoder) #vocab size
	print("xkcd encoding data finished")

	#encode all strings in each of these arrays
	(trX1, trX2, trY), (vaX1, vaX2, vaY), (teX1, teX2, teY) = encode_dataset(mnli(data_dir, ordinal), encoder=text_encoder) #mnli returns tuple or tuples ((),(),())
	
	#output: unpadded lists of word indices
	#special tokens used for converting from prem, hypothesis to a long vector of the form _start_prem_delimiter_hypothesis_classify_
	#note that each of these special tokens maps to a unique integer since len(encoder) is growing
	encoder['_start_'] = len(encoder) 
	encoder['_delimiter_'] = len(encoder)
	encoder['_classify_'] = len(encoder)
	clf_token = encoder['_classify_']

	#number of special characters
	n_special = 3 #track the number of characters we have added to the vocabulary

	max_len = n_ctx//2-2
	#get max length of story + answer in train, val, test
	#take min of (that + 3), n_ctx
	#the 3 is to take care of start, delimiter, end tokens
	print("xkcd determining n_ctx, max length in characters")
	n_ctx = min(max([len(x1[:max_len])+len(x2[:max_len]) for x1, x2 in zip(trX1, trX2)]+[len(x1[:max_len])+len(x2[:max_len]) for x1, x2 in zip(vaX1, vaX2)]+[len(x1[:max_len])+len(x2[:max_len]) for x1, x2 in zip(teX1, teX2)])+n_special, n_ctx)
	print("initial n_ctx: ", n_ctx) #78
	trX, trM = transform_mnli(trX1, trX2) #combine training prem and hypothesis into one string seperated by delimiters (_start_prem_delimiter_hypothesis_classify_)
	vaX, vaM = transform_mnli(vaX1, vaX2) #same as above but for dev
	#trM and vaM are masks but unsure what that means or what they are for
	if submit:
		teX, teM = transform_mnli(teX1, teX2)

	n_train = len(trY)
	n_valid = len(vaY)
	n_batch_train = n_batch*n_gpu  
	n_updates_total = (n_train//n_batch_train)*n_iter #total number of updates over all iterations (epochs)

	#data placeholders
	print("xkcd setting structure of model to accept input")
	#specify the variables and their shapes which will 
	X_train = tf.placeholder(tf.int32, [n_batch_train, n_ctx, 2])
	
	M_train = tf.placeholder(tf.float32, [n_batch_train, n_ctx])
	X = tf.placeholder(tf.int32, [None, n_ctx, 2])
	M = tf.placeholder(tf.float32, [None, n_ctx])
	Y_train = tf.placeholder(tf.int32, [n_batch_train])
	Y = tf.placeholder(tf.int32, [None])

	#train setup
	print("xkcd setting up training and loss functions")
	train, logits, clf_losses, lm_losses = mgpu_train(X_train, M_train, Y_train, dataset=args['dataset'], ordinal=args['ordinal'])

	clf_loss = tf.reduce_mean(clf_losses)

	params = find_trainable_variables('model')
	print("2nd check params [0] length: ",params[:n_transfer][0].get_shape()) #(40993, 768)

	sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
	sess.run(tf.global_variables_initializer())

	#load saved params
	print("xkcd loading saved parameters")
	shapes = json.load(open('model/params_shapes.json'))
	offsets = np.cumsum([np.prod(shape) for shape in shapes])
	init_params = [np.load('model/params_{}.npy'.format(n)) for n in range(10)]
	init_params = np.split(np.concatenate(init_params, 0), offsets)[:-1]
	init_params = [param.reshape(shape) for param, shape in zip(init_params, shapes)]
	init_params[0] = init_params[0][:n_ctx]
	init_params[0] = np.concatenate([init_params[1], (np.random.randn(n_special, n_embd)*0.02).astype(np.float32), init_params[0]], 0)
	del init_params[1]
	if n_transfer == -1:
		n_transfer = 0
	else:
		n_transfer = 1+n_transfer*12
	print("initparams[0] length: ",len(init_params[:n_transfer][0])) #40559, 1
	print("initparams length: ",len(init_params[:n_transfer])) #145
	print("params [0] length: ",params[:n_transfer][0].get_shape()) #(40993, 768)
	print("params length: ",len(params[:n_transfer])) #145

	
	print("init params[0] type",type(init_params[:n_transfer][0]))#ndarray
	print("init params type",type(init_params[:n_transfer]))#list
	print("params type",type(params[:n_transfer]))#list
	

	#initialize weights from transformer weights
	sess.run([p.assign(ip) for p, ip in zip(params[:n_transfer], init_params[:n_transfer])])

	#more setup
	print("xkcd more model setup, setting up model loss logs, etc")
	eval_mgpu_logits, eval_mgpu_clf_losses, eval_mgpu_lm_losses = mgpu_predict(X_train, M_train, Y_train, dataset=args['dataset'], ordinal=args['ordinal'])
	eval_logits, eval_clf_losses, eval_lm_losses = model_mnli(X, M, Y, train=False, reuse=True, ordinal=args['ordinal'])
	eval_clf_loss = tf.reduce_mean(eval_clf_losses)
	eval_mgpu_clf_loss = tf.reduce_mean(eval_mgpu_clf_losses)

	n_updates = 0
	n_epochs = 0

	if submit:
		save(os.path.join(save_dir, desc, 'best_params.jl'))
	best_score = 0
	# main training epoch loop
	print("xkcd starting training epoch loop")
	for i in range(n_iter):
		print("Epoch ",i)
		#xmb, mmb, ymb for x mult batch
		for xmb, mmb, ymb in iter_data(*shuffle(trX, trM, trY, random_state=np.random), n_batch=n_batch_train, truncate=True, verbose=True):
			cost, _ = sess.run([clf_loss, train], {X_train:xmb, M_train:mmb, Y_train:ymb})
			n_updates += 1
			if n_updates in [1000, 2000, 4000, 8000, 16000, 32000] and n_epochs == 0:
				#for first epoch only, if on these updates, log. Maybe to check on progress? 
				log()
		n_epochs += 1
		log()

	print('total training time in hours:\t',str(timedelta(seconds=(timer() - startTime))))

	#do predictions
	if submit:
		sess.run([p.assign(ip) for p, ip in zip(params, joblib.load(os.path.join(save_dir, desc, 'best_params.jl')))])
		predict(args['test']) #predict on testing data if args.test==True (default value is True),predict on validation set if args.test=False
		if analysis:
			analy_fn = analyses[dataset]
			analy_fn(data_dir, os.path.join(submission_dir, filenames[dataset]),log_file, test=args['test'], ordinal=args['ordinal'])
	

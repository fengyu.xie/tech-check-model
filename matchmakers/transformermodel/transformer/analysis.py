import os
import json
import numpy as np
import pandas as pd

from sklearn.metrics import accuracy_score, confusion_matrix

from datasets import read_mnlis


def mnli(data_dir, pred_path, log_path, test=False, ordinal=False):
    preds = pd.read_csv(pred_path, delimiter='\t')['prediction'].values.tolist()
    fold = 'test' if test else 'dev'
    _, _, labels = read_mnlis(os.path.join(data_dir, f'mnli_{fold}_feats.jsonl'), ordinal)
    # _, _, labels = read_mnlis(os.path.join(data_dir, 'mnli_dev_feats.jsonl'), ordinal)
    acc = accuracy_score(labels, preds)*100.
    logs = [json.loads(line) for line in open(log_path)][1:]
    best_validation_index = np.argmax([log['va_acc'] for log in logs])
    val_acc = logs[best_validation_index]['va_acc']
    print("mnli valid acc: %2.2f" % val_acc)
    print("mnli %s acc: %2.2f" % (fold, acc))
    print(confusion_matrix(labels, preds))


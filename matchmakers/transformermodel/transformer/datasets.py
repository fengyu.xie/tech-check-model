import os
import json
import numpy as np


def read_mnlis(path, ordinal):
    """
    Helper function to read .jsonl file
    Returns tuple of lists
    """
    with open(path,'r',encoding='utf-8-sig') as f:
        prems = []
        hyps = []
        y = []
        lines=f.readlines()
        for line in lines:
            example=json.loads(line)
            label=example['label' if ordinal else 'bin_label']
            if label==None:
                continue
            prems.append(example['sentence1'])
            hyps.append(example['sentence2'])
            y.append(int(label))
        return prems, hyps, y


def mnli(data_dir, ordinal):
    tr_prems, tr_hyps, trY = read_mnlis(os.path.join(data_dir, 'mnli_train_feats.jsonl'), ordinal)
    dv_prems, dv_hyps, dvY = read_mnlis(os.path.join(data_dir, 'mnli_dev_feats.jsonl'), ordinal)
    te_prems, te_hyps, teY = read_mnlis(os.path.join(data_dir, 'mnli_test_feats.jsonl'), ordinal)
    # te_prems, te_hyps, teY = read_mnlis(os.path.join(data_dir, 'mnli_dev_feats.jsonl'), ordinal)
    trY = np.asarray(trY, dtype=np.int32)
    dvY = np.asarray(dvY, dtype=np.int32)
    teY = np.asarray(teY, dtype=np.int32)
    return (tr_prems, tr_hyps, trY), (dv_prems, dv_hyps, dvY), (te_prems, te_hyps, teY)


## Transformer Model
As stated in `matchmakers/` *README*, our transformer model is an `Matchmaker` implementation of OpenAI's Transformer model. Our code (`transformerwrapper.py`) primarily differs from their [repository](https://github.com/openai/finetune-transformer-lm) in two ways: 
1. OpenAI's code is a data processing script meant for one-time use. It reads in a train-dev-test split and accordingly, fine tunes their pretrained model on the training set, validates it on the development set, predicts on a test set, and produces a confusion matrix of those predictions. The OpenAI code saves the best weights of the trained model in `transformer/save/`, but does not provide functionality to load in the model again. Our code attempts to apportion the above functionality into an object-oriented framework implementing the `Matchmaker` abstract base class. 
2. OpenAI's code uses their pre-trained model to solve a text prediction problem on the ROCStories dataset. James Mullenbach's fork of OpenAI's repository repurposes the OpenAI code and pretrained model to perform text classification on the Stanford NLI dataset. We adapt his code for the Multigenre NLI (MNLI) dataset. 

#### Datasets 
We fine tune OpenAI's pretrained Transformer on MNLI 1.0, a textual entailment dataset which labels sentence pairs as neutral, contradiction, or entailment--a three class classification problem. See below for more details on MNLI 1.0 and why we chose it. 

Since we would like to return claims if they are entailed by/contradict database fact-checks, we also bin contradiction and entailment into one category to create a binary classification problem. Thus, in the .jsonl files, 'label' corresponds to neutral/contradiction/entailment, while 'bin_label' corresponds to non-match (neutral) and match (contradiction/entailment). 

The original [MNLI 1.0 corpus](https://www.nyu.edu/projects/bowman/multinli/paper.pdf) can be downloaded [here](https://www.nyu.edu/projects/bowman/multinli/), and includes a training set of ~393k and two development sets--matched and mismatched--of ~10k each. At the time of this Data+ project (Summer 2018), no labelled test set is available online because the MNLI dataset is part of a Kaggle competition.

The following four .jsonl files in `transformermodel/` were processed using `mnli2json.py`:  
- mnli_train_feats.jsonl
- mnli_dev_feats.jsonl: This is the matched development set from the MNLI 1.0 corpus. 
- mnli_test_feats.jsonl: Downloaded from the [Kaggle MultiNLI Matched](https://www.kaggle.com/c/multinli-matched-open-evaluation) competition. This is an unlabelled test set of ~9.7k.
- mnli_test_hard.jsonl: Downloaded from the [Kaggle MultiNLI Matched Hard](https://www.kaggle.com/c/multinli-matched-open-hard-evaluation) competition. This is an ~4k selected subset of the above MNLI test set.  

#### Predictions
Our predictions on mnli_dev_feats,  mnli_test_feats, and mnli_test_hard can be found in `transformermodel/transformer/submission/`. All three-class predictions were made using the saved model weights in `transformermodel/transformer/save/mnli_nonbin`, whereas all binary predictions were made using `transformermodel/transformer/save/mnli_bin/`. 
- mnli_dev_bin.tsv: Our binary predictions on mnli_dev_feats.
- mnli_dev_nonbin.tsv: Our three-class predictions on mnli_dev_feats.
- mnli_hard.tsv/.csv: Our three-class predictions on mnli_test_hard. We submitted these predictions to the Kaggle MultiNLI Matched Hard competition; the .csv version is formatted using their sentence pair IDs and required formatting. 
- mnli_preds.tsv/.csv: Our three-class predictions on mnli_test_feats. We submitted these predictions to the Kaggle MultiNLI Matched competition; the .csv version is formatted using their sentence pair IDs and required formatting. 

#### Training Runs
We trained our Transformer model on a Google Cloud virtual machine with 8 NVIDIA k80 GPUs, 8 vCPUs, and 64gB RAM, and saved all models to `transformermodel/transformer/save`. Each training run on the 393k MNLI training set took approximately 9.5 hrs. Due to time and budget constraints, we did not tune model hyperparameters to achieve better results. 

The results of the two models we trained are below. We obtained test set accuracies by submitting our three-class model to the Kaggle MultiNLI competitions. The username of our submissions is 'anonymous'; our team name is 'anonymous76' for the Matched competition and 'anonymous' for the Matched Hard competition. As of 08/01/18, we are ranked 4th and 2nd respectively on those competitions. 
 
| Model             | Test set | Hard test set | Dev set |   
|-------------------|----------|---------------|---------|
| /save/mnli_nonbin | 81.645%  | 73.007%       | 82.04% |   
| /save/mnli_bin    |  -       |  -            | 85.28   |   

#### Code
*Disclaimer: `transformerwrapper.py` is a work-in-progress. Though functional, much remains to be done, as we outline below. Furthermore, because all the code in `transformer/` came from OpenAI and was uncommented, any comments in that code and the documentation below is only our best understanding of it.*

##### `transformerwrapper.py`
##### Initialization
As an instance of the `Matchmaker` class, `TransformerWrapper` implements the required trainModel(), compareTwo(), and matchSentence_db() methods. `TransformerWrapper` can be initialized with a dictionary of class attributes which the user may want to change (i.e. directories to read data from and write predictions/saved models to, learning rate, optimization algorithms, number of GPUs, etc.). The default values of these attributes are shown below: 

```python
args={'desc': 'current_run', #name for the run
	  'dataset': 'mnli',
	  'log_dir': 'transformer/log/',
	  'save_dir': 'transformer/save/',
	  'data_dir': '', #current directory
	  'submission_dir': 'transformer/submission/',
	  'submit': True, #flag to get test predictions
	  'ordinal': True, #if flag set to true, will get 3 class prediction instead of binary
	  'test': True, #flag to run on test
	  'seed': 42, #random seed
	  'n_iter': 3, #epochs
	  'n_batch': 8, #number in batch per gpu
	  'max_grad_norm': 1,
	  'lr': 6.25e-5,
	  'lr_warmup':.002 ,
	  'n_ctx': 512, #if longest input is longer than this, crop it to this size
	  'n_embd':768, #embedding size
	  'n_head': 12, #attention heads
	  'n_layer': 12, #transformer blocks
	  'embd_pdrop': 0.1,
	  'attn_pdrop': 0.1,
	  'resid_pdrop': 0.1,
	  'clf_pdrop': 0.1,
	  'l2': 0.01,
	  'vector_l2': True, 
	  'n_gpu': 1,
	  'opt': 'adam', #optimization algorithm
	  'afn':'gelu', #activation fcn
	  'lr_schedule':'warmup_linear',
	  'encoder_path':'transformer/model/encoder_bpe_40000.json',
	  'bpe_path':'transformer/model/vocab_40000.bpe',
	  'n_transfer':12, #transformer blocks to keep fixed (default to all)
	  'lm_coef':0.5,
	  'b1':0.9,
	  'b2':0.999,
	  'e':1e-8,
	  }
```
One can initialize a `TransformerWrapper` by passing in a dictionary of the attributes one wants to change: 
```python
argdict={'dataset': 'mnli', 
	 'desc': 'mnli_bin',
	 'ordinal': False,
	 'n_gpu': 8,
		}
model=TransformerWrapper(**argdict)
```
##### The `MNLIModel` class 
The Transformer model architecture described in the OpenAI paper is initialized by calling the `model_mnli()` method of an `MNLIModel` object (which is currently created in `TransformerWrapper`'s `init()`). Using TensorFlow syntax, the `model_mnli()` method sets up the **computational graph**. 
Currently, this function is called by `mgpu_train()` or `mgpu_predict()`. As the names suggest, these functions set up a TensorFlow computational graph across the number of GPUs specified by `TransformerWrapper`'s n_gpu attribute. 

##### Training a model
Train/dev/test datasets are set in `transformer/datasets.py`, which is called by the `encodeData()` function. Before training on these datasets, the `trainModel()` method loads OpenAI's pretrained paramters (saved in `transformer/model`) into the computational graph. The boolean attribute `TransformerWrapper.ordinal` controls whether binary or three-class training/predictions will be performed. The best parameters in a training run will be saved to `transformer/save`, and validation scores are saved to `transformer/log`.

##### Loading a saved model
Since the `runAnalysis()`,`compareTwo()`, and `matchSentence_db` always loads in a saved model, it should not be necessary to explicitly load one in. The path to this saved model is `save_dir/desc/best_params.jl` (class attributes) and can be found in `loadModel()`.

##### Deeper explanation of training/loading models
To make predictions on a test set or a validation set with a model, `mgpu_predict()` must be called to set up a non-trainable computational graph. However, if a trainable computational graph had already been created earlier in the code (for instance in `trainModel()`, `mgpu_train()` is called before `mgpu_predict` and does precisely that), `mgpu_predict()` must reuse the variables created by the previous computational graph, because otherwise `mgpu_predict()` tries to initialize already-existing variables, and TensorFlow throws an error. We control this behavior in `mgpu_predict()` by setting a boolean attribute  `TransformerWrapper.existing_graph`, which has a default value of False.

Similarly, `loadParams()` (loads in saved model parameters) requires identifying a computational graph if one exists and creating one if it doesn't. `TransformerWrapper.existing_graph` also controls this behavior. 

##### Analyzing predictions
The `runAnalysis()` function predicts on the test set, saving those predictions to a .tsv file in `transformer/submission`, and computes a confusion matrix for those predictions.   

##### Plugging `TransformerWrapper` into the Pipeline
Currently on a Windows PC (Intel i5 quad core CPU, no GPUs, 8gB RAM), it takes 217 minutes to run 1 sentence against the ~6000 English, political claims in the ShareTheFacts database. The goal is for any matching algorithm to take no more than 1 minute to run a sentence against the database, so one possible future objective would be to optimize this. However, on a Google cloud virtual machine with 8 GPUs, 8 vCPUs, and 64gB RAM, making the ~9800 predictions for the MNLI test set took only 5 minutes. Though the `iter_predict()` method *should* be able to parallelize the batch predictions on multiple GPUs (so for 8 GPUs, and a batch size of 8, the model should be predicting on 64 at a time), at this time, that part of the code does not work, which is why it is contained in an if `False` statement. This suggests a bug with the `mgpu_predict()` method, though the similar parallel batch-training functionality of the `mgpu_train()` method works. 

Effectively then, the Google VM was performing these computations without its GPUs, or only 1 GPU (unclear which). So the optimization route with the greatest effort-to-results ratio may be to fix `iter_predict()` and enable parallel predicting with multiple GPUs. 


##### `transformer/transformer.py`
This module contains James Mullenbach's adaptation of OpenAI's Transformer repository, with the minor adaptations of deleting the bash scripts and changing the dataset used from SNLI to MNLI. It runs on the same backend modules as `transformerwrapper.py` and is a valuable, working, source example for debugging `transformerwrapper.py`.  

##### `transformer/analysis.py`
The `analysis()` function is imported as `mnli_analysis()` in `transformerwrapper.py` and called by `runAnalysis()`. It reads in .tsv prediction files from `transformer/submission`, the labels from the original MNLI .jsonl datasets, and the best validation accuracy of the model from training logs. It prints the validation accuracy of the model, the accuracy score of the prediction file, and a confusion matrix for the prediction file. 

##### `transformer/datasets.py`
The `mnli()` function is imported by `transformerwrapper.py`,  reads in the training, development, and testing .jsonl data files, and returns all three datasets in a tuple of nested lists. Currently, the code is set up so that one must always read in and encode all three datasets to train the model, or get encoded predictions to train on. **We recommend altering this function so that it reads in and returns only one dataset at a time, and changing the code in `transformerwrapper.py` accordingly, so that one can load in files to predict on independently of the training/development datasets.**

##### `transformer/opt.py`
Contains OpenAI's implementations of the Adam optimization algorithm, and wrapper functions for various TensorFlow warmup schedules (cosine, constant, and linear). Note that although OpenAI provides three warmup schedules, only cosine is actually used in this code. 

##### `transformer/text_utils.py`
Contains the `TextEncoder` class, which uses functions from the spaCy package and custom text cleaning functions to tokenize/standardize input text. The TextEncoder object uses `transformer/model/encoder_bpe_40000.json` and `transformer/model/vocab40000.bpe` to encode input sentences using the vocabulary of 40k word parts. 

##### `transformer/utils.py`
Contains a suite of general helper functions such as a dataset encoding function, and functions to set up TensorFlow multi-GPU training. 

##### Known issues: 
- The confidence value that `compareTwo()` returns is not a probability. It is simply the raw value that the model output for the predicted class. For example, if three-class model outputs [-1.7345, .342, .147] when comparing sentences A and B, then `compareTwo()` will predict class 1 (contradiction) and return .342 as the confidence score. 
- The `mnli()` function from datasets.py reads in train/dev/test training sets, and returns all three. As mentioned above, we suggest altering this function so that it reads in/returns only one at a time. 
- The `mnli()` function from datasets.py hard-codes the names of the datasets. We suggest making dataset names class attributes of `TransformerWrapper` and passing them into the `mnli()` function.
- The multi-GPU predicting functionality of `iter_predict()` is currently contained in an if `False` because it doesn't work. See section 'Plugging `TransformerWrapper` into the Pipeline'. 

#### Why we chose MNLI
There are several other commonly used NLI datasets including SciTail, RTE, SICK, and SNLI. Of these, the first 3 datasets all have fewer than 30,000 labeled sentence pairs and are thus too small for our purposes. SNLI has 570,000 labeled sentence pairs and is actually larger than MNLI, but a closer examination of both data sets reveals that SNLI's contradiction examples do not suit our purpose. To generate contradiction examples, the SNLI authors showed Amazon Mech Turk the following text

> We will show you the caption for a photo. We will not show you the photo. Using only the caption and what you know about the world... write one alternate caption that is definitely a false description of the photo. Example: For the caption “Two dogs are running through a field.” you could write “The pets are sitting on a couch.” This is different from the maybe correct category because it’s impossible for the dogs to be both running and sitting.

The existence of the "photo", even if it is not shown, indicates a shared context which is not the case in political discourse. For example, SNLI gives the following as a prime example of contradiction:

- Sentence 1: A black race car starts up in front of a crowd of people.
- Sentence 2: A man is driving down a lonely road.

While it is unlikely that both sentences are true in the same photo, *it is certainly possible for both to be true*, as far as we know men keep driving down lonely roads even if somewhere else in the world a black race car is starting in front of a crowd. Nearly all of SNLI's contradictions follow this pattern and would therefore be considered neutral under our definition of contradiction. Although MNLI is modeled on SNLI, the way in which the authors generate their data bypasses this problem. 

In conclusion,  we chose MNLI because it is the largest dataset whose labels agree with our definitions.

You will notice that MNLI has two dev and test sets: MNLI matched and MNLI mismatched, the difference being that MNLI mismatched includes sentence pairs in which the two sentences come from different genres (i.e. one sentence comes from a telephone conversation and the other from a novel) whereas in MNLI matched sentences are only compared to other sentences from the same genre. Since all of our sentences come from the same "genre" (political discourse), MNLI matched is more representative of our needs. 

Finally, we use MNLI 1.0 but it does not matter which version you use. From the MNLI [website](https://www.nyu.edu/projects/bowman/multinli/):

> MultiNLI 0.9 differs from MultiNLI 1.0 only in the pairID and promptID fields in the training and development sets (and the attached paper), so results achieved on version 0.9 are still valid on 1.0.

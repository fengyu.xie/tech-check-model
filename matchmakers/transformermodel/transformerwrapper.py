from timeit import default_timer as timer
from datetime import timedelta

import numpy as np
import tensorflow as tf
import json
import joblib
import math
import random
import argparse

from os import path
from functools import partial
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score

import sys
import os
basepath=os.path.dirname(__file__) #path of current script
commonpath=os.path.abspath(os.path.join(basepath,"..","..","common"))
transformerpath=os.path.abspath(os.path.join(basepath, "transformer"))
matchpath=os.path.abspath(os.path.join(basepath, ".."))
sys.path.insert(0, commonpath)
sys.path.insert(0, transformerpath)
sys.path.insert(0, matchpath)
from matchmaker import Matchmaker
from utils import PostgreSQLDB
from match import Match
from opt import adam, warmup_cosine, warmup_linear, warmup_constant
from datasets import mnli
from analysis import mnli  as mnli_analysis
from text_utils import TextEncoder
from transformer_utils import encode_dataset, flatten, iter_data, find_trainable_variables, get_ema_vars, convert_gradient_to_tensor, shape_list, ResultLogger, assign_to_gpu, average_grads, make_path

class TransformerWrapper(Matchmaker):

	def set_args(self,**kwargs):
		"""
		Returns a dictionary of all the arguments
		"""
		args={
			  'desc': 'mnli_bin', #name for the run
			  'dataset': 'mnli',
			  'verbose': True, #controls output of tqdm of iter_data
			  'log_dir': path.abspath(path.join(basepath,'transformer','log')),
			  'save_dir': path.abspath(path.join(basepath,'transformer','save')),
			  'data_dir': basepath,
			  'submission_dir': path.abspath(path.join(basepath,'transformer','submission')),
			  'submit': True, #flag to get test predictions
			  'ordinal': False, #if flag set to true, will get 3 class prediction instead of binary
			  'test': True, #flag to run on test
			  'seed': 42, #random seed
			  'n_iter': 3, #epochs
			  'n_batch': 8, #number in batch per gpu
			  'max_grad_norm': 1,
			  'lr': 6.25e-5,
			  'lr_warmup':.002 ,
			  'n_ctx': 281, #sentences are cropped to this many characters, only change this if you really really really know what you are doing i.e. you retrained a model with a different n_ctx
			  'n_embd':768, #embedding size
			  'n_head': 12, #attention heads
			  'n_layer': 12, #transformer blocks
			  'embd_pdrop': 0.1,
			  'attn_pdrop': 0.1,
			  'resid_pdrop': 0.1,
			  'clf_pdrop': 0.1,
			  'l2': 0.01,
			  'vector_l2': True,
			  'n_gpu': 1,
			  'opt': 'adam', #optimization algorithm
			  'afn':'gelu', #activation fcn
			  'lr_schedule':'warmup_linear',
			  'encoder_path': path.abspath(path.join(basepath,'transformer','model','encoder_bpe_40000.json')),
			  'bpe_path': path.abspath(path.join(basepath,'transformer','model','vocab_40000.bpe')),
			  'n_transfer':12, #transformer blocks to keep fixed (default to all)
			  'lm_coef':0.5,
			  'b1':0.9,
			  'b2':0.999,
			  'e':1e-8,
			  }
		self.args=args
		#overwrite args with passed args where applicable
		for k,v in kwargs.items():
			args[k]=v
		for k in args.keys():
			setattr(self,k,args[k])


	def get_model_args(self, model_arglist):
		"""
		Returns dictionary model_args reflecting TransformerWrapper object's current state
		"""
		return {k: getattr(self, k) for k in model_arglist}

	def __init__(self, **kwargs):
		#see get_args in transformer.py for names of all keywords
		super().__init__() #call super constructor
		self.db=PostgreSQLDB('techcheck')
		self.set_args(**kwargs) #initializing constants args
		random.seed(self.seed)
		np.random.seed(self.seed)
		tf.set_random_seed(self.seed)

		self.opt_fns = {
			'adam':adam,
		}

		self.lr_schedules = {
			'warmup_cosine':warmup_cosine,
			'warmup_linear':warmup_linear,
			'warmup_constant':warmup_constant,
		}

		self.log_file = os.path.join(self.log_dir, '{}.jsonl'.format(self.desc))
		print("set up encoders")
		self.text_encoder = TextEncoder(self.encoder_path, self.bpe_path) #set up the encoder
		self.encoder = self.text_encoder.encoder  #store encoder (dictionary from token to number) (unclear where numbers come from)
		self.n_vocab = len(self.encoder) #vocab size

		#output: unpadded lists of word indices
		#special tokens used for converting from prem, hypothesis to a long vector of the form _start_prem_delimiter_hypothesis_classify_
		#note that each of these special tokens maps to a unique integer since len(encoder) is growing
		self.encoder['_start_'] = len(self.encoder)
		self.encoder['_delimiter_'] = len(self.encoder)
		self.encoder['_classify_'] = len(self.encoder)
		self.clf_token = self.encoder['_classify_']
		#number of special characters
		self.n_special= 3 #track the number of characters we have added to the vocabulary
		self.max_len = self.n_ctx//2-2
		#get max length of story + answer in train, val, test
		#take min of (that + 3), n_ctx
		#the 3 is to take care of start, delimiter, end tokens

		self.model_arglist = ["n_vocab",
							 "n_special",
							 "n_ctx",
							 "n_embd",
							 "n_head",
							 "attn_pdrop",
							 "embd_pdrop",
							 "resid_pdrop",
							 "n_layer",
							 "clf_pdrop",
							 "clf_token",
							 "ordinal",
							 "afn"
							]

		self.modelconfig={'mnli': lambda x: MNLIModel(self.get_model_args(x))} #every time model config is called with current dataset, mnli model is initialized from true values of arglist above
		self.best_score=0
		self.n_batch_train= self.n_batch*self.n_gpu

		self.pred_fns = {
			'mnli': lambda x:np.argmax(x, 1)
		}

		self.filenames = {
			# 'mnli': 'mnli_preds.tsv'
			'mnli': 'mnli_dev_bin.tsv'
		}

		if self.ordinal:
			self.label_decoders = {
				'mnli':{0: 'neutral',
						1: 'contradiction',
						2: 'entailment'}
			}

		else:
			self.label_decoders = {
				'mnli': None
			}

		self.analyses = {
			'mnli': mnli_analysis
		 }

		self.existing_graph=False


	def transform_mnli(self,X1, X2):
		"""
		:param X1, X2: lists of lists
		Glue claims together with delimiter and stuff, and add position tokens
		:returns tuple of numpy arrays
		"""
		n_batch = len(X1)
		xmb = np.zeros((n_batch, self.n_ctx, 2), dtype=np.int32)
		mmb = np.zeros((n_batch, self.n_ctx), dtype=np.float32)
		start = self.encoder['_start_']
		delimiter = self.encoder['_delimiter_']
		for i, (x1, x2) in enumerate(zip(X1, X2)):
			#concatenate
			x = [start] + x1[:self.max_len]+[delimiter]+x2[:self.max_len]+[self.clf_token]
			l = len(x)
			#set np array
			xmb[i,:l,0] = x
			#mask
			mmb[i,:l] = 1
		#position tokens
		xmb[:,:,1] = np.arange(self.n_vocab+self.n_special, self.n_vocab+self.n_special+self.n_ctx)
		return xmb, mmb


	def encodeData(self):
		print("encoding begun")
		#encode all strings in each of these arrays
		#types: X1, X2 are lists; Y is nd array
		(trX1, trX2, self.trY), (vaX1, vaX2, self.vaY), (teX1, teX2, self.teY) = encode_dataset(mnli(self.data_dir, self.ordinal), encoder=self.text_encoder,verbose=self.verbose) #mnli returns tuple or tuples ((),(),())
		print("encoding finished")

		print("determining n_ctx, max length in characters")
		self.n_ctx= min(max([len(x1[:self.max_len])+len(x2[:self.max_len]) for x1, x2 in zip(trX1, trX2)]+[len(x1[:self.max_len])+len(x2[:self.max_len]) for x1, x2 in zip(vaX1, vaX2)]+[len(x1[:self.max_len])+len(x2[:self.max_len]) for x1, x2 in zip(teX1, teX2)])+self.n_special, self.n_ctx)

		self.trX, self.trM = self.transform_mnli(trX1, trX2) #combine training prem and hypothesis into one string seperated by delimiters (_start_prem_delimiter_hypothesis_classify_)
		self.vaX, self.vaM = self.transform_mnli(vaX1, vaX2) #same as above but for dev
		#trM and vaM are masks but unsure what that means or what they are for
		if self.submit:
			self.teX, self.teM = self.transform_mnli(teX1, teX2)

	def set_placeholders(self):
		self.X_train = tf.placeholder(tf.int32, [self.n_batch_train, self.n_ctx, 2])
		self.M_train = tf.placeholder(tf.float32, [self.n_batch_train, self.n_ctx])
		self.X = tf.placeholder(tf.int32, [None, self.n_ctx, 2])
		self.M = tf.placeholder(tf.float32, [None, self.n_ctx])

		self.Y_train = tf.placeholder(tf.int32, [self.n_batch_train])
		self.Y = tf.placeholder(tf.int32, [None])


	def mgpu_train(self, **kwargs): #mgpu for multiple gpu
		"""
		See the argparse in main for the kwargs.
		This function initializes the computational graph for training.
		It splits the training data (xs) by number of gpus, assigns the data
		to a gpu. Each gpu tower calls the training method. At the end, the gradients from
		the gpus are averaged and used to update weights.Returns the training function.
		"""
		xs = [self.X_train, self.M_train, self.Y_train]
		gpu_ops = []
		gpu_grads = []
		xs = (tf.split(x, self.n_gpu, 0) for x in xs)
		for i, xs in enumerate(zip(*xs)):
			do_reuse = True if i > 0 else None

			with tf.device(assign_to_gpu(i, "/gpu:0")), tf.variable_scope(tf.get_variable_scope(), reuse=do_reuse):
				clf_logits, clf_losses, lm_losses = self.modelconfig[self.dataset](self.model_arglist).model_mnli(self.X_train, self.M_train,self.Y_train, train=True, reuse=do_reuse, ordinal=self.ordinal)
				if self.lm_coef > 0:
					train_loss = tf.reduce_mean(clf_losses) + self.lm_coef*tf.reduce_mean(lm_losses)
				else:
					train_loss = tf.reduce_mean(clf_losses)
				params = find_trainable_variables("model")
				grads = tf.gradients(train_loss, params)
				grads = list(zip(grads, params))
				gpu_grads.append(grads)
				gpu_ops.append([clf_logits, clf_losses, lm_losses])
		ops = [tf.concat(op, 0) for op in zip(*gpu_ops)]
		grads = average_grads(gpu_grads)
		grads = [g for g, p in grads]
		train = self.opt_fns[self.opt](params, grads, self.lr, partial(self.lr_schedules[self.lr_schedule], warmup=self.lr_warmup), self.n_updates_total, l2=self.l2, max_grad_norm=self.max_grad_norm, vector_l2=self.vector_l2, b1=self.b1, b2=self.b2, e=self.e)
		return [train]+ops

	def mgpu_predict(self,**kwargs):
		#if graph doesn't exist, need the reuse=None.
		"""
		This function enables prediction using multiple gpus
		"""
		xs = [self.X_train, self.M_train, self.Y_train]

		gpu_ops = []
		xs = (tf.split(x, self.n_gpu, 0) for x in xs)
		for i, xs in enumerate(zip(*xs)):
			#note that the reuse parameter of model_mnli() cannot be set to False, only None, because  of a tensorflow quirk
			do_reuse = True if (i > 0 or self.existing_graph) else None
			with tf.device(assign_to_gpu(i, "/gpu:0")), tf.variable_scope(tf.get_variable_scope(), reuse=do_reuse):
				if self.dataset == 'mnli':
					clf_logits, clf_losses, lm_losses = self.modelconfig[self.dataset](self.model_arglist).model_mnli(self.X_train, self.M_train,self.Y_train, train=False, reuse=do_reuse, ordinal=self.ordinal)
				gpu_ops.append([clf_logits, clf_losses, lm_losses])
		ops = [tf.concat(op, 0) for op in zip(*gpu_ops)]
		return ops

	def loadParams(self,param_values=None,param_limit=None):
		"""
		Initializes computational graph variables, andoads params in to the computational graph.
		"""
		self.params = find_trainable_variables('model') #type list
		self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
		self.sess.run(tf.global_variables_initializer()) #initialize the variables in params
		#load trained values of variables in params to
		self.sess.run([p.assign(ip) for p, ip in zip(self.params[:param_limit], param_values[:param_limit])])


	def iter_apply(self,Xs, Ms, Ys):
		fns = [lambda x:np.concatenate(x, 0), lambda x:float(np.sum(x))]
		results = []
		for xmb, mmb, ymb in iter_data(Xs, Ms, Ys, n_batch=self.n_batch_train, truncate=False, verbose=self.verbose):
			n = len(xmb)
			if n == self.n_batch_train:
				res = self.sess.run([self.eval_mgpu_logits, self.eval_mgpu_clf_loss], {self.X_train:xmb, self.M_train:mmb, self.Y_train:ymb})
			else:
				res = self.sess.run([self.eval_logits, self.eval_clf_loss], {self.X:xmb, self.M:mmb, self.Y:ymb})
			res = [r*n for r in res]
			results.append(res)
		results = zip(*results)
		return [fn(res) for res, fn in zip(results, fns)]

	def iter_predict(self,Xs, Ms):
		"""
		Iterates through Xs and Ms and makes predictions
		:param Xs: any iterable with bracket based indexing
		:param Ms: same as above
		:returns a np array of prediction results
		"""
		logits = []
		for xmb, mmb in iter_data(Xs, Ms, n_batch=self.n_batch_train, truncate=False, verbose=self.verbose):
			n = len(xmb)
			if False:
			#if n == self.n_batch_train: #the original code
				res=self.sess.run(self.eval_mgpu_logits, {self.X_train:xmb, self.M_train:mmb})
				# print("expected length 64 of res: ", len(res))
				logits.append(res)
			else:
				res=self.sess.run(self.eval_logits, {self.X:xmb, self.M:mmb})
				# print("expected length 64 of res: ", len(res))
				logits.append(res)
		logits = np.concatenate(logits, 0)
		return logits

	def save(path):
		ps = self.sess.run(self.params)
		joblib.dump(ps, make_path(path))

	def log(self):
		tr_logits, tr_cost = self.iter_apply(self.trX[:self.n_valid], self.trM[:self.n_valid], self.trY[:self.n_valid])
		va_logits, va_cost = self.iter_apply(self.vaX, self.vaM, self.vaY)
		tr_cost = tr_cost/len(self.trY[:self.n_valid])
		va_cost = va_cost/self.n_valid
		tr_acc = accuracy_score(self.trY[:self.n_valid], np.argmax(tr_logits, 1))*100.
		va_acc = accuracy_score(self.vaY, np.argmax(va_logits, 1))*100.
		self.logger.log(n_epochs=self.n_epochs, n_updates=self.n_updates, tr_cost=tr_cost, va_cost=va_cost, tr_acc=tr_acc, va_acc=va_acc)
		print('%d %d %.3f %.3f %.2f %.2f'%(self.n_epochs, self.n_updates, tr_cost, va_cost, tr_acc, va_acc))
		if self.submit:
			score = va_acc
			print("current score: ", score)
			if score > self.best_score:
				self.best_score = score
				self.save(os.path.join(self.save_dir, self.desc, 'best_params.jl'))

	def trainModel(self):
		self.encodeData()
		startTime=timer()
		self.n_train= len(self.trY)
		self.n_valid= len(self.vaY)
		self.n_updates_total= (self.n_train// self.n_batch_train)*self.n_iter #total number of updates over all iterations (epochs)

		#data placeholders
		print("setting structure of model to accept input")
		self.set_placeholders()

		print("setting up training")
		train, logits, clf_losses, lm_losses = self.mgpu_train(dataset=self.dataset, ordinal=self.ordinal)
		clf_loss = tf.reduce_mean(clf_losses)
		self.existing_graph=True

		#load saved params
		print("loading saved parameters")
		shapes = json.load(open('transformer/model/params_shapes.json'))
		offsets = np.cumsum([np.prod(shape) for shape in shapes])
		init_params = [np.load('transformer/model/params_{}.npy'.format(n)) for n in range(10)]
		init_params = np.split(np.concatenate(init_params, 0), offsets)[:-1]
		init_params = [param.reshape(shape) for param, shape in zip(init_params, shapes)]
		init_params[0] = init_params[0][:self.n_ctx]
		init_params[0] = np.concatenate([init_params[1], (np.random.randn(self.n_special, self.n_embd)*0.02).astype(np.float32), init_params[0]], 0)
		del init_params[1]

		if self.n_transfer == -1:
			self.n_transfer = 0
		else:
			self.n_transfer = 1+self.n_transfer*12

		# self.params = find_trainable_variables('model') #type list
		# self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True))
		# self.sess.run(tf.global_variables_initializer())

		# initialize weights from transformer weights
		self.loadParams(param_values=init_params,param_limit=self.n_transfer)
		#more setup
		print("more model setup, setting up model loss logs, etc")
		self.eval_mgpu_logits, eval_mgpu_clf_losses, eval_mgpu_lm_losses = self.mgpu_predict(dataset=self.dataset, ordinal=self.ordinal)
		self.eval_logits, eval_clf_losses, eval_lm_losses = self.modelconfig[self.dataset](self.model_arglist).model_mnli(self.X, self.M, self.Y, train=False, reuse=True, ordinal=self.ordinal)
		self.eval_clf_loss = tf.reduce_mean(eval_clf_losses)
		self.eval_mgpu_clf_loss = tf.reduce_mean(eval_mgpu_clf_losses)

		self.n_updates = 0
		self.n_epochs = 0
		# return
		# main training epoch loop
		print("starting training epoch loop")
		#initializing the logger in the training loop, because calling ResultLogger overwrites anything  currently
		#in the log file, which we don't want to do unless we're training again.
		self.logger = ResultLogger(path=self.log_file, **self.args) #set up logging and log parser parameters

		for i in range(self.n_iter):
			print("Epoch ",i+1)
			#xmb, mmb, ymb for x multiple batch?
			for xmb, mmb, ymb in iter_data(*shuffle(self.trX, self.trM, self.trY, random_state=np.random), n_batch=self.n_batch_train, truncate=True, verbose=self.verbose):
				cost, _ = self.sess.run([clf_loss, train], {self.X_train:xmb, self.M_train:mmb, self.Y_train:ymb})
				self.n_updates += 1
				if self.n_updates in [1000, 2000, 4000, 8000, 16000, 32000] and self.n_epochs == 0:
					#for first epoch only, if on these updates, log. Maybe to check on progress?
					self.log() #updates best_score and saves best params
			print("Best score for epoch ",self.n_epochs,": ",self.best_score)
			self.n_epochs += 1
			self.log()
		print('total training time in hours:\t',str(timedelta(seconds=(timer() - startTime))))


	def setup_compgraph(self):
		"""
		Sets up computational graph
		"""
		self.set_placeholders()
		self.eval_mgpu_logits, eval_mgpu_clf_losses, eval_mgpu_lm_losses = self.mgpu_predict(dataset=self.dataset, ordinal=self.ordinal)
		self.eval_logits, eval_clf_losses, eval_lm_losses = self.modelconfig[self.dataset](self.model_arglist).model_mnli(self.X, self.M, self.Y, train=False, reuse=True, ordinal=self.ordinal)

	def loadModel(self):
		if not self.existing_graph:
			self.setup_compgraph() #setup graph structure
		self.loadParams(param_values=joblib.load(os.path.join(self.save_dir, self.desc, 'best_params.jl')))

	def predict(self):
		"""
		Writes predictions to the submission directory, currently only works after running trainModel
		"""
		filename = self.filenames[self.dataset]
		pred_fn = self.pred_fns[self.dataset]
		label_decoder = self.label_decoders[self.dataset]
		starttime=timer()
		if self.test:
			print("predicting on test set")
			print(len(self.teX),len(self.teM))
			predictions = pred_fn(self.iter_predict(self.teX, self.teM)) #predict on test data
		else:
			print("predict on  validation set")
			predictions = pred_fn(self.iter_predict(self.vaX, self.vaM)) #predict on validation data
		print("time to predict:  ",timer()-starttime)
		labels=["None" for prediction in predictions]
		if label_decoder is not None:
			labels = [label_decoder[prediction] for prediction in predictions]
		path = os.path.join(self.submission_dir, filename)
		os.makedirs(os.path.dirname(path), exist_ok=True)
		with open(path, 'w') as f:
			f.write('{}\t{}\t{}\n'.format('pairID', 'prediction', 'gold_label'))
			for i, (prediction,label) in enumerate(zip(predictions,labels)):
				f.write('{}\t{}\t{}\n'.format(i, prediction,label))


	def runAnalysis(self):
		"""
		Loads saved parameters into existing computational graph
		Writes prediction on dataset to file, and prints analysis
		"""
		print("Running analysis...")
		self.loadModel()
		self.predict()
		analy_fn = self.analyses[self.dataset]
		analy_fn(self.data_dir, os.path.join(self.submission_dir, self.filenames[self.dataset]),self.log_file, test=self.test, ordinal=self.ordinal)


	def compareTwo(self, claim1, claim2, loadmodel=True):
		#encoders initialized in init
		if False:
			self.loadModel()

		teX1=self.text_encoder.encode([claim1],verbose=self.verbose)
		teX2=self.text_encoder.encode([claim2],verbose=self.verbose)

		teX,teM=self.transform_mnli(teX1,teX2) #hopefully this works
		classpreds=self.iter_predict(teX, teM)
		fakeconfidence=np.amax(classpreds)

		pred_fn=self.pred_fns[self.dataset]
		prediction=pred_fn(classpreds)

		return (claim1, claim2, fakeconfidence, prediction)
		#for mnli, the pred_fn is a simple np argmax

	def matchSentence_db(self, sentence):
		'''
		find all matches for a given sentence by iterating through the database
		:param sentence: the sentence to be checked against the database
		:returns: a list of tuples of the form (original sentence, matching sentence, confidence)
		'''
		start=timer()
		# self.loadModel()
		# print("time to load a model:",timer()- start)
		results=[]
		db_sentences=[]
		#sqlResult=self.db.execute("""SELECT DISTINCT "statement","id" FROM facts.facts WHERE "language_id"=1 AND "organization_id" IN (1,2,3,10,14,15,31,37);""")
		sqlResult=self.db.execute("""SELECT DISTINCT "statement","id" FROM facts.facts WHERE "language_id"=1 AND "organization_id" IN (1,2,3,10,14,15,31,37) LIMIT 100;""")
		for row in sqlResult.fetchall():
			db_sentences.append(row[0])
		del sqlResult

		premises=encode_dataset([[db_sentences]],encoder=self.text_encoder,verbose=self.verbose)[0][0]
		print("time to load/encode 100 dbsen",timer()-start)

		encoded_sentence=self.text_encoder.encode([sentence],verbose=self.verbose)[0]
		hypotheses=[encoded_sentence]*len(premises)

		start=timer()
		X,M=self.transform_mnli(premises,hypotheses)
		print("time for transform_mnli",timer()-start)
		start=timer()
		classpreds=self.iter_predict(X,M)
		print("time to predict",timer()-start)

		pred_fn = self.pred_fns[self.dataset]
		predictions=pred_fn(classpreds)

		for i,prediction in enumerate(predictions):
			if prediction==1:
				results.append(Match(live_sentence=sentence, db_sentence=db_sentences[i],confidence=classpreds[i]))
		print("time to match 1 sentence: ", timer()-start)
		return results

	def onFinish(self):
		'''
		do not need to do anything on finish
		'''
		pass

class MNLIModel():
	@staticmethod
	def gelu(x):
		return 0.5*x*(1+tf.tanh(math.sqrt(2/math.pi)*(x+0.044715*tf.pow(x, 3))))

	@staticmethod
	def swish(x):
		return x*tf.nn.sigmoid(x)

	def updateAttr(self,model_args):
		"""
		Sometimes the MNLI model args are updated outside of TransFormerModel's init, so that
		the MNLIModel object's attributes must be updated as well. Doubles as an attribute
		setter function.
		"""
		for k in model_args.keys():
			setattr(self,k,model_args[k])


	def __init__(self,model_args):
		self.updateAttr(model_args)
		self.act_fns = {'relu':tf.nn.relu,
					   'swish':MNLIModel.swish,
					   'gelu':MNLIModel.gelu
					  }

	@staticmethod
	def _norm(x, g=None, b=None, e=1e-5, axis=[1]):
		u = tf.reduce_mean(x, axis=axis, keepdims=True)
		s = tf.reduce_mean(tf.square(x-u), axis=axis, keepdims=True)
		x = (x - u) * tf.rsqrt(s + e)
		if g is not None and b is not None:
			x = x*g + b
		return x

	@staticmethod
	def norm(x, scope, axis=[-1]):
		with tf.variable_scope(scope):
			n_state = shape_list(x)[-1]
			g = tf.get_variable("g", [n_state], initializer=tf.constant_initializer(1))
			b = tf.get_variable("b", [n_state], initializer=tf.constant_initializer(0))
			g, b = get_ema_vars(g, b)
			return MNLIModel._norm(x, g, b, axis=axis)

	@staticmethod
	def dropout(x, pdrop, train):
		if train and pdrop > 0:
			x = tf.nn.dropout(x, 1-pdrop)
		return x

	@staticmethod
	def mask_attn_weights(w):
		n = shape_list(w)[-1]
		b = tf.matrix_band_part(tf.ones([n, n]), -1, 0)
		b = tf.reshape(b, [1, 1, n, n])
		w = w*b + -1e9*(1-b)
		return w

	@staticmethod
	def _attn(q, k, v, attn_pdrop,train=False, scale=False):
		w = tf.matmul(q, k)

		if scale:
			n_state = shape_list(v)[-1]
			w = w*tf.rsqrt(tf.cast(n_state, tf.float32))

		w = MNLIModel.mask_attn_weights(w)
		w = tf.nn.softmax(w)

		w = MNLIModel.dropout(w, attn_pdrop, train)

		a = tf.matmul(w, v)
		return a

	@staticmethod
	def split_states(x, n):
		x_shape = shape_list(x)
		m = x_shape[-1]
		new_x_shape = x_shape[:-1]+[n, m//n]
		return tf.reshape(x, new_x_shape)

	@staticmethod
	def merge_states(x):
		x_shape = shape_list(x)
		new_x_shape = x_shape[:-2]+[np.prod(x_shape[-2:])]
		return tf.reshape(x, new_x_shape)

	@staticmethod
	def split_heads(x, n, k=False):
		if k:
			return tf.transpose(MNLIModel.split_states(x, n), [0, 2, 3, 1])
		else:
			return tf.transpose(MNLIModel.split_states(x, n), [0, 2, 1, 3])

	@staticmethod
	def merge_heads(x):
		return MNLIModel.merge_states(tf.transpose(x, [0, 2, 1, 3]))

	@staticmethod
	def conv1d(x, scope, nf, rf, w_init=tf.random_normal_initializer(stddev=0.02), b_init=tf.constant_initializer(0), pad='VALID', train=False):
		with tf.variable_scope(scope):
			nx = shape_list(x)[-1]
			w = tf.get_variable("w", [rf, nx, nf], initializer=w_init)
			b = tf.get_variable("b", [nf], initializer=b_init)
			if rf == 1: #faster 1x1 conv
				c = tf.reshape(tf.matmul(tf.reshape(x, [-1, nx]), tf.reshape(w, [-1, nf]))+b, shape_list(x)[:-1]+[nf])
			else: #was used to train LM
				c = tf.nn.conv1d(x, w, stride=1, padding=pad)+b
			return c
	@staticmethod
	def attn(x, scope, n_state, n_head, attn_pdrop, resid_pdrop,train=False, scale=False):
		assert n_state%n_head==0
		with tf.variable_scope(scope):
			c = MNLIModel.conv1d(x, 'c_attn', n_state*3, 1, train=train)
			q, k, v = tf.split(c, 3, 2)
			q = MNLIModel.split_heads(q, n_head)
			k = MNLIModel.split_heads(k, n_head, k=True)
			v = MNLIModel.split_heads(v, n_head)
			a = MNLIModel._attn(q, k, v, attn_pdrop, train=train, scale=scale)
			a = MNLIModel.merge_heads(a)
			a = MNLIModel.conv1d(a, 'c_proj', n_state, 1, train=train)
			a = MNLIModel.dropout(a, resid_pdrop, train)
			return a

	def mlp(self, x, scope, n_state, train=False):
		with tf.variable_scope(scope):
			nx = shape_list(x)[-1]
			act = self.act_fns[self.afn]
			h = act(MNLIModel.conv1d(x, 'c_fc', n_state, 1, train=train))
			h2 = MNLIModel.conv1d(h, 'c_proj', nx, 1, train=train)
			h2 = MNLIModel.dropout(h2, self.resid_pdrop, train)
			return h2

	def block(self,x, scope,train=False, scale=False):
		with tf.variable_scope(scope):
			nx = shape_list(x)[-1]
			a = MNLIModel.attn(x, 'attn', nx, self.n_head, self.attn_pdrop,self.resid_pdrop,train=train, scale=scale)
			n = MNLIModel.norm(x+a, 'ln_1')
			m = self.mlp(n, 'mlp', nx*4, train=train)
			h = MNLIModel.norm(n+m, 'ln_2')
			return h

	@staticmethod
	def embed(X, we):
		we = convert_gradient_to_tensor(we)
		e = tf.gather(we, X)
		h = tf.reduce_sum(e, 2)
		return h

	@staticmethod
	def clf_mnli(x, w_init=tf.random_normal_initializer(stddev=0.02), b_init=tf.constant_initializer(0), train=False, ordinal=False):
		out_sz = 3 if ordinal else 2 #out size is number of categories, if in ordinal mode this is 3 (entailment,contradiction,neutral), otherwise 2 (match, non-match)
		#final linear layer, for single input
		with tf.variable_scope('clf_mnli'):
			nx = shape_list(x)[-1]
			w = tf.get_variable('w', [nx, out_sz], initializer=w_init)
			b = tf.get_variable('b', [out_sz], initializer=b_init)
			return tf.matmul(x, w)+b


	#MAIN MODELS
	def model_mnli(self,X, M, Y, train=False, reuse=False, ordinal=False):
		"""
			X: [batch, n_ctx, 2]
			M: [batch, n_ctx]
			Y: [batch]
		"""
		with tf.variable_scope('model', reuse=reuse):
			we = tf.get_variable("we", [self.n_vocab+self.n_special+self.n_ctx, self.n_embd], initializer=tf.random_normal_initializer(stddev=0.02))
			we = MNLIModel.dropout(we, self.embd_pdrop, train)

			#transformer blocks
			h = MNLIModel.embed(X, we)
			for layer in range(self.n_layer):
				h = self.block(h, 'h%d'%layer,train=train, scale=True)
			#language modeling objective
			lm_h = tf.reshape(h[:, :-1], [-1, self.n_embd])
			lm_logits = tf.matmul(lm_h, we, transpose_b=True)
			lm_losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=lm_logits, labels=tf.reshape(X[:, 1:, 0], [-1]))
			lm_losses = tf.reshape(lm_losses, [shape_list(X)[0], shape_list(X)[1]-1])
			lm_losses = tf.reduce_sum(lm_losses*M[:, 1:], 1)/tf.reduce_sum(M[:, 1:], 1)

			clf_h = tf.reshape(h, [-1, self.n_embd])
			#get length of each example
			pool_idx = tf.cast(tf.argmax(tf.cast(tf.equal(X[:, :, 0], self.clf_token), tf.float32), 1), tf.int32)
			#just takes the state of the transformer at the end of the input, I think?
			clf_h = tf.gather(clf_h, tf.range(shape_list(X)[0], dtype=tf.int32)*self.n_ctx+pool_idx)

			#reshape to [batch, embed size]
			clf_h = tf.reshape(clf_h, [-1, 1, self.n_embd])
			if train and self.clf_pdrop > 0:
				shape = shape_list(clf_h)
				shape[1] = 1
				clf_h = tf.nn.dropout(clf_h, 1-self.clf_pdrop, shape)
			#put tensor back into (batch, embed size) shape
			clf_h = tf.reshape(clf_h, [-1, self.n_embd])
			#linear layer
			clf_logits = MNLIModel.clf_mnli(clf_h, train=train, ordinal=self.ordinal)

			#final softmax
			clf_losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=clf_logits, labels=Y)

			return clf_logits, clf_losses, lm_losses



if __name__ == '__main__':
	argdict={
		# 'dataset': 'mnli',
		# 'desc': 'mnli_bin',
		# 'ordinal': False,
		# 'n_gpu': 8,
		'verbose': False
		}

	model=TransformerWrapper(**argdict)
#	analy_fn = model.analyses[model.dataset]
#	analy_fn(model.data_dir, os.path.join(model.submission_dir, model.filenames[model.dataset]),model.log_file, test=model.test, ordinal=model.ordinal)
#	sys.exit(0)
	# model.trainModel()
#	model.runAnalysis()
	# sys.exit(0)

	claim1="The U.S. lost 500 billion in trade to China."
	claim2="The U.S. lost 1 trillion in trade to China."
	for matchobj in model.matchSentence_db(claim1):
		print(matchobj.db_sentence)
#	print(str(model.compareTwo(claim1,claim2))) #returns match object

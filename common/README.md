## Common
The common folder holds files which are part of the engine but are either shared by more than one module or don't really belong in any particular module. This allows us to keep our other folders cleaner. 

#### googlecloudcredentials.json
Contains the credentials for Google Cloud Platform. More information on setting up authentication can be found [here](https://cloud.google.com/speech-to-text/docs/reference/libraries#client-libraries-install-python). On a linux machine I recommend adding the line ```export GOOGLE_APPLICATION_CREDENTIALS = "/home/vcm/techcheck/engine/common/googlecloudcredentials.json"``` to ```~.bashrc``` which is a bash script run each time you open a shell.

#### makerequest.py
Contains `makeGetRequest` method which makes intelligent get requests. This method should be called rather than directly using the requests module. For example, this code is used when calling the ClaimBuster API

#### matchmaker.py
See README in matchmakers folder for information on matchmakers

#### misc.py
File for miscellaneous functions for general use. Currently contains method for logging to the `#errors` channel on the tech & check slack. This method is currently not being used

#### obamasotu2016.txt
Transcript of Obama's 2016 State of the Union address

#### requirements.txt
 A list of required packages. To install all packages required for the model simply execute ```pip install -r requirements.txt``` in your shell

#### trumpsotu.txt
Contains the transcript of Trump's 2018 State of the Union address. This is a good text for testing models because the Share the Facts database has many fact checks relevant to this text (often word for word quotes)

#### utils.py
Methods for interacting with the PostgreSQL database from python
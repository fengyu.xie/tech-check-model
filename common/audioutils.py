import os
import json
import subprocess
from os import path
from random import randint
from time import time as secondsSinceEpoch

'''
create a new audio file by cropping a given file

:param filename: path to the file to crop
:param resourcesPath: path to the folder where cropped file should be put
:param startTime: where in the file the crop should begin in seconds
:param secondsLimit: number of seconds to take (takes [startTime,startTime+secondsLimit]) - note that this may not be exact
:param verbose: if true, prints ffmpeg output
:returns: absolute path to cropped file
'''
def cropAudio(filename, resourcesPath, startTime, secondsLimit, verbose=False):
    newFilename=path.abspath(path.join(resourcesPath,generateFileName('.flac'))) #get new filename for output

    #use ffmpeg in command line to crop file
    #command: ffmpeg -ss [startTime] -t [secondsLimit] -y -i [fileName] [newFileName]
    #-ss [startTime] specifies the timestamp (in seconds) at which to begin the crop
    #-t [secondsLimit] specifies how many seconds to take
    #-y specifies automatic overwriting to prevent getting stuck
    #-i [filename] specifies input file (we let ffmpeg infer input file format)
    #[newFilename] specifies output file
    with open(os.devnull, 'w') as devnull: #devnull is a sink which discards all output (and is very fast to write to)
        #send both stdout and stderr to devnull (ffmpeg writes a lot to both)
        out=None if verbose else devnull #print to stdout,stderr if verbose, otherwise send output to devnull
        subprocess.run(['ffmpeg','-ss',str(startTime),'-t',str(secondsLimit),'-y','-i',filename,newFilename], stdout=out, stderr=out)
    return newFilename

'''
extract audio from a video and encode as mono track FLAC audio with given rate

:param filename: path to the video file
:param resourcesPath: path to the folder where audio file should be put
:param rate: desired bitrate
:param verbose: if true, prints ffmpeg output
:returns: absolute path to audio file
'''
def videoToFlac(fileName, resourcesPath, rate=16000, verbose=False):
    newFileName=path.abspath(path.join(resourcesPath,generateFileName('.flac'))) #get file to write out to

    #use ffmpeg in command line to convert file
    #command: ffmpeg -y -i [fileName] -ac 1 -f s16le -acodec pcm_s16le -ar [rateString] [newFileName]
    #-y specifies automatic overwriting to prevent getting stuck
    #-i specifies input file (we let ffmpeg infer input file format)
    #ac -1 specifies to convert to single channel audio
    #-f s16le specifies output encoding
    #-acodec pcm_s16le specifies codec to use for conversion
    with open(os.devnull, 'w') as devnull: #devnull is a sink which discards all output (and is very fast to write to)
        #convert from rate to a string to pass to ffmpeg 
        rateString=str(int(rate/1000))+'k' #16000 => 16k

        out=None if verbose else devnull #print to stdout,stderr if verbose, otherwise send output to devnull
        subprocess.run(['ffmpeg','-y','-i',fileName,'-ac','1','-c:a','flac','-ar',rateString,newFileName], stdout=out, stderr=out)

    return newFileName

def videoToLinear16(fileName, resourcesPath, rate=16000, verbose=False):
    newFileName=path.abspath(path.join(resourcesPath,generateFileName('.wav'))) #get file to write out to

    #use ffmpeg in command line to convert file
    #command: ffmpeg -y -i [fileName] -ac 1 -f s16le -acodec pcm_s16le -ar [rateString] [newFileName]
    #-y specifies automatic overwriting to prevent getting stuck
    #-i specifies input file (we let ffmpeg infer input file format)
    #ac -1 specifies to convert to single channel audio
    #-f s16le specifies output encoding
    #-acodec pcm_s16le specifies codec to use for conversion
    with open(os.devnull, 'w') as devnull: #devnull is a sink which discards all output (and is very fast to write to)
        #convert from rate to a string to pass to ffmpeg 
        rateString=str(int(rate/1000))+'k' #16000 => 16k

        out=None if verbose else devnull #print to stdout,stderr if verbose, otherwise send output to devnull
        subprocess.run(['ffmpeg','-y','-i',fileName,'-ac','1','-f','s16le','-acodec','pcm_s16le','-ar',rateString,newFileName], stdout=out, stderr=out)

    return newFileName

'''
get the length of an audio or video file in seconds using ffprobe

:param filename: abs path to the file whose length is desired
:param resourcesPath: abs path to a folder to write temporary files to
:returns: the length of the file in seconds
'''
def getAudioLength(filename, resourcesPath):
    tempFile=path.abspath(path.join(resourcesPath,generateFileName('.txt'))) #get temporary file to write to

    #use ffprobe to write duration to a file
    #command: ffprobe -i [fileName] -show_entries format=duration -v quiet -of json
    #-i filename specifies input file (we let ffmpeg infer input file format)
    #-show_entires format=duration shows format.duration (the length in seconds)
    #-v quiet no output other than format.duration
    #-of json specifies output format of json
    #result is {"format":{"duration":[len]}}
    with open(tempFile, 'w') as f: #write to temp file
        subprocess.run(['ffprobe','-i',filename,'-show_entries','format=duration','-v','quiet','-of','json'], stdout=f, stderr=f)
    
    #read duration from file
    with open(tempFile, 'r') as f:
        duration=json.loads(f.read())['format']['duration']
    
    os.remove(tempFile) #delete temporary file
    
    return float(duration) #cast from string to float and return

'''
generate a filename designed to be distinct from other filenames generated by this method within the past day

:param: the file extension (i.e. .mp3, .txt, .wav, etc)
:returns: a valid file name which with high probability is distinct from all other filenames generated by this method in the past day 
'''
def generateFileName(extension):
    sec=secondsSinceEpoch() #start with the number of seconds since the epoch
    sec=sec % 100000 #most of the digits are going to be the same so just keep enough digits to distinguish between days (there are 86,400 s/day)
    sec=int(sec*100000) #don't want a decimal because it will mess up ffmpeg so multiply by 10000 to be precise to the nearest .00001 seconds and truncate
    sec=sec*100+randint(10,99) #finally, add 2 randomly generated digits in case two files are created at the same time to the nearest 10 microseconds
    out=str(sec) #convert to string
    #sec will in most cases be 12 digits but may be shorter due to leading 0s so append leading 0s to get to 12 digits
    while len(out)<12: 
        out='0'+out #append leading 0

    #add extension
    if not extension.startswith('.'):
        out+='.' #add dot before extension if none was provided
    return out+extension #append extension
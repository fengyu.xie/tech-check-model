import sys
import os
basepath=os.getcwd() #path of current script
commonpath=os.path.abspath(os.path.join(basepath,"common"))
modelpath = os.path.abspath(os.path.join(basepath, "matchmakers/transformermodel"))
transformerpath=os.path.abspath(os.path.join(modelpath, "transformer"))
sys.path.insert(0, commonpath)
sys.path.insert(0, transformerpath)
sys.path.insert(0, modelpath)

from transformerwrapper import TransformerWrapper

class matcherAPI:

    def __init__(self, model, sc):
#        self.model = TransformerWrapper()
        self.loaded = False

    def compareTwo(self, claim1, claim2):
        '''compare two sentences according to the loaded parameters and return their scores'''
        if not self.loaded:
            self.model.loadModel()
            self.loaded = True
        claim1, claim2, fakeconfidence, prediction = self.model.compareTwo(claim1, claim2)
        return fakeconfidence, prediction

    def matchSentence(self, claim):
        '''match a sentence to the database based on some filter'''
        if not self.loaded:
            self.model.loadModel()
            self.loaded = True
        results = self.model.matchSentence_db(claim)
        return results

if __name__ == "__main__":
    model = matcherAPI()
    claim1 = "Since the election, we have created 2.4 million new jobs, including 200,000 new jobs in manufacturing alone."
    # claim2 = "Oh my god how I adore this subfreezing piece of high calories content cream"
    result = model.matchSentence(claim1)
    # print("fakeconfidence", fakeconfidence)
    # print("prediction", prediction)
    for r in result:
        print(r.toDict()[db_sentence])

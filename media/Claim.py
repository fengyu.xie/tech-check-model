from operator import itemgetter
import json

'''
store claims in a standardized form for easy creation and retrieval
'''
class Claim:
    def __init__(self, claim, timestamp=None, sttConfidence=None, claimbusterScore=None, matches=[], meetsThresholds=True):
        self.claim=claim
        self.timestamp=timestamp
        self.sttConfidence=sttConfidence
        self.claimbusterScore=claimbusterScore
        self.matches=matches
        self.meetsThresholds=meetsThresholds

    #naive override of str method, to do this more formally may need to also override ___repr__
    '''
    overwrite the str() method to print Claim objects in a nice format
    '''
    def __str__(self):
        out=self.claim
        out+='\n\ttimestamp:\t\t'+str(self.timestamp)
        out+='\n\tsttConfidence:\t\t'+str(self.sttConfidence)
        out+='\n\tclaimbusterScore:\t'+str(self.claimbusterScore)
        out+='\n\tmeetsthresholds:\t'+str(self.meetsThresholds)
        if self.matches:
            out+='\n\tfound '+str(len(self.matches))+' matches. Best match:'
            for match in sorted(self.matches, key=itemgetter(2), reverse=True)[:1]:
                out+='\n\t\t'+str(match[1])
                out+='\n\t\tConfidence: '+str(round(match[2],2))
        else:
            out+='\n\tfound 0 matches'
        return out

    '''
    Return claim object in dictionary form. Currently only used by the flask app for passing Claims through sockets

    :return: a dictionary 
    '''
    def toDict(self):
        return {'claim':self.claim,'matches':[match.toDict() for match in self.matches]} #need to recursively call toDict() on Match objects
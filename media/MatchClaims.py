from queue import Queue
from threading import Thread

#this class is very simple, it's real purpose is to take care of multithreading so that Matchmaker objects don't have to
class ClaimMatcher(Thread):
    def __init__(self, claimQueue, finishedQueue, model, sc):
        Thread.__init__(self) #call super constructor

        self.claimQueue=claimQueue          #read in queue
        self.finishedQueue=finishedQueue    #write out queue
        self.model=model                    #a Matchmaker object used for finding matches, the model is passed in to give ultimate flexibility for testing
    
        self.done=False        #kill switch
        self.flushing=False    #indicator if currently flushing
        
    def run(self):
        while not self.done:
            claimObj=self.claimQueue.get() #get next element
            
            #check for flush
            if self.flushing:
                #consume queue until flush end indicator is reached
                while not claimObj==self.FLUSH_END:
                    claimObj=self.claimQueue.get()
                self.flushing=False #indicate flush is done
                continue

            if claimObj:
                #only get matches for claims which meet the thresholds, otherwise just put directly in finishedQueue
                if claimObj.meetsThresholds:    
                    #use the Matchmaker object given to find matches for the claim
                    claimObj.matches=self.model.matchSentence_db(claimObj.claim)

                if not self.flushing:
                    #put claim in write out queue
                    self.finishedQueue.put(claimObj)
        
        #finished so call onFinish event handler
        self.model.onFinish()

    '''
    gracefully and safely kill thread
    '''
    def gracefulKill(self):
        self.done=True
        self.claimQueue.put(None) #release blocking
    
    '''
    flush queue
    '''
    def flush(self):
        self.flushing=True #indicate flush
        self.FLUSH_END='[FLUSH_END]' #constant put in the queue which indicates the end of the flush
        self.claimQueue.put(self.FLUSH_END) #release blocking

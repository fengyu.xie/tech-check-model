import os
from threading import Thread
from queue import Queue
from time import sleep
from nltk import sent_tokenize
from google.cloud import speech_v1p1beta1 as speech #google speech to text wrapper
from google.api_core.exceptions import OutOfRange
from google.api_core.exceptions import Unknown

from Claim import Claim

class StreamTranscriber(Thread):
    def __init__(self, queueQueue, textQueue, transcriptionThreshold, returnAllResults, rate):
        Thread.__init__(self) #call super constructor

        self.queueQueue=queueQueue #read in
        self.textQueue=textQueue #write out
        self.transcriptionThreshold=transcriptionThreshold
        self.returnAllResults=returnAllResults
        self.rate=rate

        #prepare for google speech api calls
        self.client = speech.SpeechClient()

        config = speech.types.RecognitionConfig(
            encoding=speech.enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=self.rate,        #specify sample rate used   
            language_code='en-US',              #audio is english
            max_alternatives=1,                 #only want most confident result
            profanity_filter=False,             #don't censor profanity
            enable_word_time_offsets=False,     #if desired can get the start and end time signature of each individual word
            enable_automatic_punctuation=True   #want punctuation (currently beta)
            )

        self.streamingConfig = speech.types.StreamingRecognitionConfig(
            config=config,
            interim_results=False)
 
    def run(self):
        self.done=False
        while not self.done:
            q=self.queueQueue.get() #get next queue to read from
            if q: #None may be put into queue for graceful killing purposes
                g=self.queueToGenerator(q) #convert queue to generator

                #every time we get content in our audioGenerator, map it to a StreamingRecognizeRequest 
                requests = (speech.types.StreamingRecognizeRequest(audio_content=content) for content in g) #similar to list comprehension but for generators
                responses = self.client.streaming_recognize(self.streamingConfig, requests)

                try:
                    for response in responses:
                        #extract information
                        resultObj=response.results[0]
                        transcript=resultObj.alternatives[0].transcript
                        if resultObj.stability>0:
                            confidence=resultObj.stability
                        else:
                            confidence=resultObj.alternatives[0].confidence #speech to text confidence

                        #only want text that passes some threshold
                        if confidence>=self.transcriptionThreshold or self.returnAllResults:
                            #using nltk's sentence tokenizer, add each sentence to the queue
                            for sentence in sent_tokenize(transcript): #use nltk to split into a list of sentences
                                #create a Claim object and add it to the queue
                                self.textQueue.put(Claim(sentence, sttConfidence=confidence, meetsThresholds=confidence>=self.transcriptionThreshold))
                except OutOfRange: #exceeded time limit
                    #this should never happen but in case it does ensure that the thread doesnt die
                    print('exceeded time limit')
                except Unknown as e:
                    print('speech to text transcription encountered the following error:',e) #not sure why this happens so just catch and print
                except Exception as e:
                    if response.error.code:
                        #google cloud speech returned an error
                        errorCode=response.error.code 
                        if errorCode==11:
                            #the radio stream gives the 5ish seconds before you make the request very rapidly at the beginning
                            #this causes audio to be sent to google too fast
                            #if this happens simply sleep for a little to let google catch up
                            print('too fast, slowing down')
                            sleep(0.8)
                        else:
                            raise RuntimeError('[MP] unknown google speech error: '+str(response))
                    else:
                        #not a google error so just raise the original exception
                        raise e
        self.queueQueue.put(None) #help kill others
    
    def queueToGenerator(self, q):
        while not self.done:
            elem=q.get()
            if elem is None:
                break

            yield elem #yield adds elem to the generator stream
                
    def gracefulKill(self):
        self.done=True
        self.queueQueue.put(None)
        
class FileTranscriber(Thread):
    def __init__(self, fileQueue, textQueue, transcriptionThreshold, returnAllResults):
            Thread.__init__(self) #call super constructor

            self.fileQueue=fileQueue #read in
            self.textQueue=textQueue #write out
            self.transcriptionThreshold=transcriptionThreshold
            self.returnAllResults=returnAllResults

            self.config = speech.types.RecognitionConfig(
                encoding=speech.enums.RecognitionConfig.AudioEncoding.FLAC,    #specify audio encoding (supported types found at https://cloud.google.com/speech-to-text/docs/encoding)
                sample_rate_hertz=16000,
                max_alternatives=1,                                     #maximum number of alternatives to return
                profanity_filter=False,                                 #don't censor profanity
                language_code='en-US',                                  #specify that audio is english
                enable_word_time_offsets=False,                         #if desired can get the start and end time signature of each individual word
                enable_automatic_punctuation=True                       #want punctuation
            )
            
            self.client = speech.SpeechClient()
    
    def run(self):
        self.done=False
        while not self.done:
            filename=self.fileQueue.get()

            if filename:
                #read in audio data
                with open(filename, 'rb') as audioFile:
                    content=audioFile.read()
                    
                if not content: #empty audio file
                    os.remove(filename)
                    continue #move on to next file

                try:
                    #make api call
                    audio = speech.types.RecognitionAudio(content=content)
                    response = self.client.recognize(self.config, audio)

                    for result in response.results:
                        #extract information
                        transcript=result.alternatives[0].transcript
                        confidence=result.alternatives[0].confidence
                        if confidence>=self.transcriptionThreshold or self.returnAllResults:
                            #using nltk's sentence tokenizer, add each sentence to the queue
                            for sentence in sent_tokenize(transcript): #use nltk to split into a list of sentences
                                #create a Claim object and add it to the queue
                                self.textQueue.put(Claim(sentence, sttConfidence=confidence, meetsThresholds=confidence>=self.transcriptionThreshold))

                except OutOfRange: #exceeded time limit
                    #this should never happen but in case it does ensure that the thread doesnt die
                    print('exceeded time limit')
                except Unknown as e:
                    print('speech to text transcription encountered the following error:',e) #not sure why this happens so just catch and print
                
                os.remove(filename) #add back (done with file so remove)

    '''
    gracefully and safely stop thread
    '''
    def gracefulKill(self):
        self.done=True #indicate done
        self.fileQueue.put(None) #release blocking
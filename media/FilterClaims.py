from threading import Thread
from queue import Queue

#import request methods from common
import sys
from os import path
importpath=path.abspath(path.join(path.dirname(__file__),"..","..","common"))
sys.path.insert(0, importpath)
from makerequest import *

'''
filter claims based on ClaimBuster score
'''
class ClaimFilterer(Thread):
    def __init__(self, textQueue, claimQueue, threshold, returnAllResults):
        Thread.__init__(self) #call super constructor

        self.textQueue=textQueue    #queue of strings to be filitered - note that each string may consist of mutiple sentences (read in)
        self.claimQueue=claimQueue  #queue of filtered claims and their corresponding claim buster score (write out)
        self.threshold=threshold    #the mininimum ClaimBuster score required for a statement to be considered a claim
        self.returnAllResults=returnAllResults       #if true, return all sentences passed regardless of claimbuster score, otherwise claim dies

        self.done=False     #indicator specifying if done
        self.flushing=False #indicator specifying if currently in the middle of a flush

    '''
    For each string added to the textQueue, use ClaimBuster api to filter out claims, then add said claims to the claimQueue
    '''
    def run(self):
        while not self.done: #continue until thread has been killed
            claimObj=self.textQueue.get(block=True) #wait for next chunk of text

            if self.flushing:
                #consume queue until flush end indicator is reached
                while not claimObj==self.FLUSH_END:
                    claimObj=self.textQueue.get()
                self.flushing=False #indicate flush is done
                continue

            if claimObj:
                claimbusterScore=self.scoreClaim(claimObj.claim)

                #if we are returning all results, or claim acheived desired threshold, add to the output queue
                if self.returnAllResults or claimbusterScore>=self.threshold:
                    claimObj.meetsThresholds=claimbusterScore>=self.threshold #store if threshold met
                    claimObj.claimbusterScore=claimbusterScore
                    if not self.flushing: #only put in queue if not currently flushing
                        self.claimQueue.put(claimObj)

    '''
    get the claimbuster score for a given sentence

    :param sentence: a string containing a single sentence
    :returns: the claimbuster score for this sentence
    '''
    def scoreClaim(self, sentence):
        #make a call to claimbuster api
        link='http://idir-server2.uta.edu:80/factchecker/score_text/'+sentence #note that this is a get request in which the text itself is included in the url 
        validator=(lambda res: 'results' in res and res['results']) #consider the response  to be valid only the results array is existent and nonempty
        res=makeGetRequest(link, validator=validator) #make api call
    
        score=0.0 #if api call fails return score of 0
        if res:
            #note that claimbuster's sentence parser is not as good as nltk's, so claimbuster may split the sentence into multiple sentences
            #in this case we simply return the largest score given to any sentence fragment
            for result in res['results']:
                score=max(score, float(result['score'])) #only keep largest score
        
        return score
    
    '''
    gracefully and safely kill thread
    '''
    def gracefulKill(self):
        self.done=True  #indicate that thread should finish as soon as queue is empty
        self.textQueue.put(None) #add None to end of text queue to release block
    
    '''
    flush queue by removing everything currently in it
    '''
    def flush(self):
        self.flushing=True #indicate flush
        self.FLUSH_END='[FLUSH_END]' #constant put in the queue which indicates the end of the flush
        self.textQueue.put(self.FLUSH_END) #release blocking
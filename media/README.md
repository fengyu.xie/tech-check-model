# Pipeline
## Overview
The pipeline module gets claims from a source specified by the caller and finds corresponding fact checks in the Share the Facts database.

#### Design principles

The pipeline is intended to be as flexible as possible without sacrificing clarity and performance. Specifically, the pipeline is designed with 3 principles in mind:
1. Adding new sources should be as easy as possible
2. Improving the matching algorithm should not require touching any pipeline code
3. Changing out the database should not require touching any pipeline code

Among other things, this means that means that improving the pipeline can and should happen independently of model improvement. These principles are explained in more depth in [Source](#source) and [ClaimMatcher](#claimmatcher).

#### Filters
The pipeline is made up of many "filters". Each filter is given a read-in queue and a write-out queue. The filter reads in every object in the read-in queue, performs some transformation on it, then puts it in the write-out queue. Perhaps the easiest way to visualize what a filter does is the following image from computer science legend Dr. Seuss.

![image not found](https://i.ytimg.com/vi/s_V6knB0lA4/maxresdefault.jpg "sneetches")

A pipeline is simply a bunch of filters placed one after another. Each different type of audio source requires different filters. In broad terms, the MatchmakerPipeline class takes a Source object and constructs the relevant pipeline by combining filters. For example, for a stream audio source, the pipeline consists of a [StreamCollector](#streamcollector) - followed by an [AudioConverter](#audioconvertor) - followed by a [StreamSplitter](#streamsplitter) - followed by a [ClaimFilterer](#claimfilterer) - followed by a [ClaimMatcher](#claimmatcher).

#### threading.Thread
The pipeline heavily employs multithreading to improve performance. Therefore, almost all filters are subclasses of threading.Thread. The important Thread methods that you need to know are as follows:
* `start()` - starts the thread. When writing a subclass of threading.Thread, overwrite the `run()` which gets called by the `start()` method rather than overwriting the `start()` method directly. Note that a thread dies when it reaches the end of it's start method
* `join()` - wait for thread to finish. You do not need to overwrite this method

Additionally, all filters have a `gracefulKill()` method. This should stop the thread but in a graceful and safe way.

Note that there are a couple of filters which are not threads (example [BBCCollector](#bbccollector). Any filter which is not a subclass of threading.Thread must for all intents and purposes act like a thread. Specifically, they should be started by calling `run()` and must implement a `join()` method. 

All data structures that are shared by multiple threads *must* be thread safe. If you are not sure what to use, queue.Queue is typically the best bet. 

#### Running the pipeline
To run the pipeline you instantiate a `MatchmakerPipeline` object. There are 3 required arguments:

- outQueue: the queue for the pipeline to write-out to. The pipeline will fill this queue in real time as it finishes processing sentences
- source: an object of type Source specifiying the source claims should be gathered from (ex. Source.CNN)
- model: an initialized object of type `Matchmaker`

There are also many optional arguments, some of which are only required for certain sources and others of which give you more control of functionaliy.

Since `MathmakerPipeline` is a subclass of `threading.Thread`, nothing will happen until you call its `start()` method. To stop the pipeline, call the `gracefulKill()` method. Below is a very simple example of running the pipeline which prints the first 10 claims found then stops the pipeline:

```python
outQueue=Queue()
mp=MatchmakerPipeline(outQueue, Source.CNN, NLPModel()) #instantiate object
mp.start()  #start pipeline
for _ in range(10):
    print(outQueue.get()) #wait for next claim and print
mp.gracefulKill() #stop pipeline
mp.join() #wait for pipeline to stop
```

For full examples of how to run the pipeline see `TestPipeline.py` in the `interface` folder. Note that if you an encounter an exception with a message starting with '[MP]' this indicates that the expection was thrown by pipeline code not an external library

## Filters
This section lists all filters in a somewhat arbitrary order. Each filter is associated with the type of objects it expects in its read-in queue and the types of objects it puts in its read-out queue as well as a short description of the transformation performed by the filter. Note that in the codebase filters with similar functions are grouped into common files.

#### StreamCollector
> **in:** nothing

> **out:** one second chunks of mp3 encoded audio strings

The StreamCollector collects audio from an online radio stream using a [streaming get request](http://docs.python-requests.org/en/master/user/advanced/). The bitrate is inferred from the request headers.

#### MicrophoneCollector
> **in:**  nothing

> **out:** one second chunks of Linear16 encoded, single track audio strings

The MicrophoneCollector uses the PyAudio module. PyAudio gives you control over how audio is recorded and encoded so microphone audio does not need to be converted before being sent to Google. Note that microphone audio cannot be collected if the pipeline is running on a VM.

#### BBCCollector
> **in:** nothing

> **out:** Claim objects each containing one sentence of text

Since audio transcription is far from perfect, we want to use human generated subtitles whenever possible. For most audio sources this is not possible for various reasons. However, due to some intricacies in how BBC generates and publishes their subtitles, someone was able to build a system that extracts the subtitles before they are even shown on television. The subtitles are accessed through a MQTT stream. You can see a demo [here](http://test.mosquitto.org/bbc/). Currently the BBCCollector is set up to get subtitles from `bbc_news24` but `bbc_one_london` and `bbc_two_england` are also available. We found that `bbc_news24` was the only one which provided relevant news coverage. It should be noted that this method is dependent upon the MQTT stream so if the stream is no longer maintained there isn't really anything we can do. 

#### TextSplitter
> **in:** Longform text

> **out:** Claim objects, each representing one sentence of the text

Split longform text by sentence using nltk's `sent_tokenize`. Intelligent sentence recognition is required due to text such as
```
Mr. John Johnson Jr. was born in the U.S. but earned his Ph.D. in Israel  before joining Nike Inc. (www.nike.com) doing engineering (e.g. shoelace design)... but then he died.
```

#### AudioConverter
> **in:** chunks of mp3 encoded audio strings

> **out:** chunks of single track, linear16 encoded audio strings

Google Speech-to-Text is annoyingly peculiar about the [types of auto they accept](https://cloud.google.com/speech-to-text/docs/encoding). Most online radio streams are stereo mp3 streams so naturally Google neither accepts stereo audio nor mp3 encoded audio. We use ffmpeg to convert to single track Linear16 audio. ffmpeg is very powerful and we are only using a small portion of its functionality. Since ffmpeg is a command line program, we have to write the audio to a file, use ffmpeg to convert the file, then read in the converted data. All created files are placed in the `resources` folder and deleted once they are no longer needed.

#### StreamSplitter
> **in:** one second chunks of audio

> **out:** queues containing chunks of audio

Google limits the amount of audio we can send in one transcription request, so we have to split the audio to 55 seconds. One solution is to just split the audio stream into consecutive 55 second chunks. However, sentences may be split across the streams and the transcription performs best when it has full context. Therefore, we split the audio, but consecutive blocks overlap by some number of seconds. As an example, consider an overlap length of 10 seconds. The first request would be audio from 0 s to 55 s, the next from 45 s to 100 s, the third from 90 s to 145 s. It is the StreamSplitters job to split the audio appropriately, including duplicating audio that is in the overlap period. Each bock of audio to be included in the same API request is placed into its own queue. Thus, StreamSplitter produces a queue of queues.

#### StreamTranscriber
> **in:** queue containing chunks of audio

> **out:** claim objects

The StreamTranscriber uses a wrapper of Google's Speech-to-Text API. You will notice that Google's transcription is far from perfect. To understand this filter your best bet is reading Google's [documentation](https://cloud.google.com/speech-to-text/docs/reference/rpc/) of their speech-to-text service. If this filter fails, make sure that you have set up the google credentials correctly (see README in common).

#### ClaimFilterer
> **in:** claim objects

> **out:** claim objects with Claimbuster Scores

Not all sentences lend themselves to fact checking. Fortunately, a University of Texas at Arlington team lead by Dr. Chengkai Li developed the [ClaimBuster](http://idir-server2.uta.edu/claimbuster/) Claim-Spotter API which scores sentences on the degree to which they can be fact-checked. The filtering step simply takes the text objects passed to it, uses the Claim-Spotter API to get scores, and adds all claims which achieve a certain threshold score to an output queue.

#### ClaimMatcher
> **in:** claim objects
 
> **out:** claim objects with matches

ClaimMatcher is the simplest filter but also the filter where the most complicated work is done. To provide ultimate flexibility, the pipeline is not specific to a certain model. Instead, the caller passes a model (which must be a subclass of `Matchmaker`) into the pipeline. This means that changing models *does not require modifying a single line of the pipeline*. The `Matchmaker` class (in the common folder) exists to standardize all models to ensure that they can be used by the pipeline without writing any additional code. ClaimMatcher is really exists to convert a matchmaker object to a filter that can be put in the pipeline. In essence, ClaimMatcher simply calls the model's `matchSentence_db` method for every claim object in its read-in queue. 

## Misc
#### Claim
The Claim class provides a standard way to store data about a claim as it is gathered. In contrast to tuples and dictionaries, using the Claim class standardizes the key associated with each variable. Typically when an instance is initialized very little information about the Claim is known, but information is added as the Claim object travels through the pipeline. Also see `match.py` in the matchmakers folder.

#### Source
A list of accepted audio sources including the microphone. This is used for ensuring that the audio source specified is valid as well as making the addition of new sources as simple as possible. 

To add new streaming source, you have to find a server url which is streaming audio from the desired source. Usually, this involves finding an online radio broadcast then using the Chrome (or your preferred browser) developer console to find the server url of the network request. [TuneIn](https://tunein.com/) is generally a good website for finding new streaming sources. Once you have the appropriate url you simply add the source name and url to the Source enum in the following format and the source is ready to be used.
``` 
SOURCENAME={
    'url': 'url'
}
```
Note that some of the url's in the Source enum may change over time which would cause the corresponding source to stop working. To remedy this you may have to find a new server streaming the source.

To add a new type of source, do the following:
1. Add source to Source enum either with a unique string or a dictionary containing relevant information 
2. If applicable, add input validation in `MatchmakerPipeline`'s `validateInput()` method.
3. Create needed filters *only after making sure they don't already exist* (ex. you should use the `ClaimFilterer` rather than writing a new filter for ClaimBuster filtering). All filters created should follow the [guidelines for filters](#threading.thread) discussed in the introduction. 
4. In `MatchmakerPipeline`, create an initialize[Source] method in which you initialize all needed queues and filters. Make sure that the final filter writes to the `outQueue`. All filters should be added to the `self.children` array. 
5. Finally, add a condition to the `if` statement of the `init` method of `MatchmakerPipeline`, which calls your newly created initialize method if `source` is of the appropriate type

The above steps are the only changes you should need. Specifically, you should not need to touch the `run()`, `flush()`, or `gracefulKill()` methods of `MatchmakerPipeline`. Be careful of editing pre-existing filters since they may be used by other sources.

#### ./resources
ffmpeg (see [AudioConverter](#audioconverter)) is a command line tool and thus takes input and returns output in the form of files. Therefore, any audio to be converted must be written to a file, converted using ffmpeg, and read from the resulting file. These files are deleted from the system as soon as they are no longer needed but if the module is interrupted unnaturally (ex. ```ctrl-c```) these files may persist. The r
# Pipeline

## Overview

The pipeline module gets claims from a source specified by the caller and finds corresponding fact checks in the Share the Facts database.


#### Design principles


The pipeline is intended to be as flexible as possible without sacrificing clarity and performance. Specifically, the pipeline is designed with 3 principles in mind:

1. Adding new sources should be as easy as possible

2. Improving the matching algorithm should not require touching any pipeline code

3. Changing out the database should not require touching any pipeline code


Among other things, this means that means that improving the pipeline can and should happen independently of model improvement. These principles are explained in more depth in [Source](#source) and [ClaimMatcher](#claimmatcher).


#### Filters

The pipeline is made up of many "filters". Each filter is given a read-in queue and a write-out queue. The filter reads in every object in the read-in queue, performs some transformation on it, then puts it in the write-out queue. Perhaps the easiest way to visualize what a filter does is the following image from computer science legend Dr. Seuss.


![image not found](https://i.ytimg.com/vi/s_V6knB0lA4/maxresdefault.jpg  "sneetches")


A pipeline is simply a bunch of filters placed one after another. Each different type of audio source requires different filters. In broad terms, the MatchmakerPipeline class takes a Source object and constructs the relevant pipeline by combining filters. For example, for a stream audio source, the pipeline consists of a [StreamCollector](#streamcollector) - followed by an [AudioConverter](#audioconvertor) - followed by a [StreamSplitter](#streamsplitter) - followed by a [ClaimFilterer](#claimfilterer) - followed by a [ClaimMatcher](#claimmatcher).


#### threading.Thread

The pipeline heavily employs multithreading to improve performance. Therefore, almost all filters are subclasses of threading.Thread. The important Thread methods that you need to know are as follows:

*  `start()` - starts the thread. When writing a subclass of threading.Thread, overwrite the `run()` which gets called by the `start()` method rather than overwriting the `start()` method directly. Note that a thread dies when it reaches the end of it's start method

*  `join()` - wait for thread to finish. You do not need to overwrite this method


Additionally, all filters have a `gracefulKill()` method. This should stop the thread but in a graceful and safe way.


Note that there are a couple of filters which are not threads (example [BBCCollector](#bbccollector). Any filter which is not a subclass of threading.Thread must for all intents and purposes act like a thread. Specifically, they should be started by calling `run()` and must implement a `join()` method.

All data structures that are shared by multiple threads *must* be thread safe. If you are not sure what to use, queue.Queue is typically the best bet.  

#### Running the pipeline

To run the pipeline you instantiate a `MatchmakerPipeline` object. There are 3 required arguments:


- outQueue: the queue for the pipeline to write-out to. The pipeline will fill this queue in real time as it finishes processing sentences

- source: an object of type Source specifiying the source claims should be gathered from (ex. Source.CNN)

- model: an initialized object of type `Matchmaker`


There are also many optional arguments, some of which are only required for certain sources and others of which give you more control of functionaliy.


Since `MathmakerPipeline` is a subclass of `threading.Thread`, nothing will happen until you call its `start()` method. To stop the pipeline, call the `gracefulKill()` method. Below is a very simple example of running the pipeline which prints the first 10 claims found then stops the pipeline:


```python

outQueue=Queue()

mp=MatchmakerPipeline(outQueue, Source.CNN, NLPModel()) #instantiate object

mp.start() #start pipeline

for _ in  range(10):

print(outQueue.get()) #wait for next claim and print

mp.gracefulKill() #stop pipeline

mp.join() #wait for pipeline to stop

```


For full examples of how to run the pipeline see `TestPipeline.py` in the `interface` folder. Note that if you an encounter an exception with a message starting with '[MP]' this indicates that the expection was thrown by pipeline code not an external library


## Filters

This section lists all filters in a somewhat arbitrary order. Each filter is associated with the type of objects it expects in its read-in queue and the types of objects it puts in its read-out queue as well as a short description of the transformation performed by the filter. Note that in the codebase filters with similar functions are grouped into common files.


#### StreamCollector

>  **in:** nothing


>  **out:** one second chunks of mp3 encoded audio strings


The StreamCollector collects audio from an online radio stream using a [streaming get request](http://docs.python-requests.org/en/master/user/advanced/). The bitrate is inferred from the request headers.


#### MicrophoneCollector

>  **in:** nothing


>  **out:** one second chunks of Linear16 encoded, single track audio strings


The MicrophoneCollector uses the PyAudio module. PyAudio gives you control over how audio is recorded and encoded so microphone audio does not need to be converted before being sent to Google. Note that microphone audio cannot be collected if the pipeline is running on a VM.


#### BBCCollector

>  **in:** nothing


>  **out:** Claim objects each containing one sentence of text


Since audio transcription is far from perfect, we want to use human generated subtitles whenever possible. For most audio sources this is not possible for various reasons. However, due to some intricacies in how BBC generates and publishes their subtitles, someone was able to build a system that extracts the subtitles before they are even shown on television. The subtitles are accessed through a MQTT stream. You can see a demo [here](http://test.mosquitto.org/bbc/). Currently the BBCCollector is set up to get subtitles from `bbc_news24` but `bbc_one_london` and `bbc_two_england` are also available. We found that `bbc_news24` was the only one which provided relevant news coverage. It should be noted that this method is dependent upon the MQTT stream so if the stream is no longer maintained there isn't really anything we can do.

#### YoutubeCollector

>  **in:** Youtube ids (ex. https://youtu.be/ATFwMO9CebA => ATFwMO9CebA)

>  **out:** paths to flac audio files

The first step of the youtube pipeline is downloading the youtube video and extracting the audio. The video is downloaded using `youtube_dl` and the audio is extracted using `ffmpeg`. Unfortunately, there is no way to download just the audio (that I have found) so downloading the video may be slow, depending on the length. Currently, the video file is deleted after it is no longer needed but if you want to test on the same video repeatedly you may want to temporarily comment out that line so you don't have to wait for the download every time. `youtube_dl` can also be used to download subtitles associated with the video, and you may be able to get better text by doing this, but when I tried this I found that subtitles come in a wide variety of formats which are hard to convert to a straight transcript. Additionally, not all videos come with subtitles.

#### TextSplitter

>  **in:** Longform text


>  **out:** Claim objects, each representing one sentence of the text


Split longform text by sentence using nltk's `sent_tokenize`. Intelligent sentence recognition is required due to text such as

```

Mr. John Johnson Jr. was born in the U.S. but earned his Ph.D. in Israel before joining Nike Inc. (www.nike.com) doing engineering (e.g. shoelace design)... but then he died.

```


#### AudioConverter

>  **in:** chunks of mp3 encoded audio strings


>  **out:** chunks of single track, linear16 encoded audio strings


Google Speech-to-Text is annoyingly peculiar about the [types of auto they accept](https://cloud.google.com/speech-to-text/docs/encoding). Most online radio streams are stereo mp3 streams so naturally Google neither accepts stereo audio nor mp3 encoded audio. We use ffmpeg to convert to single track Linear16 audio. ffmpeg is very powerful and we are only using a small portion of its functionality. Since ffmpeg is a command line program, we have to write the audio to a file, use ffmpeg to convert the file, then read in the converted data. All created files are placed in the `resources` folder and deleted once they are no longer needed.


#### StreamSplitter

>  **in:** one second chunks of audio


>  **out:** queues containing chunks of audio


Google limits the amount of audio we can send in one transcription request, so we have to split the audio to 55 seconds. One solution is to just split the audio stream into consecutive 55 second chunks. However, sentences may be split across the streams and the transcription performs best when it has full context. Therefore, we split the audio, but consecutive blocks overlap by some number of seconds. As an example, consider an overlap length of 10 seconds. The first request would be audio from 0 s to 55 s, the next from 45 s to 100 s, the third from 90 s to 145 s. It is the StreamSplitters job to split the audio appropriately, including duplicating audio that is in the overlap period. Each bock of audio to be included in the same API request is placed into its own queue. Thus, StreamSplitter produces a queue of queues.

#### FileSplitter
>  **out:** paths to (potentially long) audio files

> **in:** paths to cropped audio files

As mentioned [above](#streamsplitter), Google limits the amount of audio we can send to the api at a time. This filter takes a large audio file, determines its length, then splits it into a series of smaller files with some overlap. For example, if `secondsLimit` is 60,`secondsOffset` is 10,  and the original file is determined to be 2 minutes and 15 seconds, the files produced will be (0s to 60s), (50s to 110s), (100s, 135s). Note that this process does not work well with Linear16 audio because ffmpeg has a lot of trouble determining length with this encoding for unknown reasons. To get around this problem I suggest only passing FLAC audio to this filter 

#### StreamTranscriber

>  **in:** queue containing chunks of audio


>  **out:** claim objects


The StreamTranscriber uses a wrapper of Google's Speech-to-Text API. You will notice that Google's transcription is far from perfect. To understand this filter your best bet is reading Google's [documentation](https://cloud.google.com/speech-to-text/docs/reference/rpc/) of their speech-to-text service. If this filter fails, make sure that you have set up the google credentials correctly (see README in common).

#### FileTranscriber
  >  **in:**  paths to FLAC audio files

>  **out:** claim objects

See [FileTranscriber](#filetranscriber) for an introduction to Google's Speech-to-Text API. This filter transcribes an entire file at a time. This method of transcription is slower than streaming transcription but tends to give higher quality transcriptions. If you find that the streaming transcription is too poor for your purposes and are willing to accommodate a longer lag, you could write streaming audio to a file and use the file transcriber. 

#### ClaimFilterer

>  **in:** claim objects


>  **out:** claim objects with Claimbuster Scores


Not all sentences lend themselves to fact checking. Fortunately, a University of Texas at Arlington team lead by Dr. Chengkai Li developed the [ClaimBuster](http://idir-server2.uta.edu/claimbuster/) Claim-Spotter API which scores sentences on the degree to which they can be fact-checked. The filtering step simply takes the text objects passed to it, uses the Claim-Spotter API to get scores, and adds all claims which achieve a certain threshold score to an output queue.


#### ClaimMatcher

>  **in:** claim objects

>  **out:** claim objects with matches


ClaimMatcher is the simplest filter but also the filter where the most complicated work is done. To provide ultimate flexibility, the pipeline is not specific to a certain model. Instead, the caller passes a model (which must be a subclass of `Matchmaker`) into the pipeline. This means that changing models *does not require modifying a single line of the pipeline*. The `Matchmaker` class (in the common folder) exists to standardize all models to ensure that they can be used by the pipeline without writing any additional code. ClaimMatcher is really exists to convert a matchmaker object to a filter that can be put in the pipeline. In essence, ClaimMatcher simply calls the model's `matchSentence_db` method for every claim object in its read-in queue.


## Misc

#### Claim

The Claim class provides a standard way to store data about a claim as it is gathered. In contrast to tuples and dictionaries, using the Claim class standardizes the key associated with each variable. Typically when an instance is initialized very little information about the Claim is known, but information is added as the Claim object travels through the pipeline. Also see `match.py` in the matchmakers folder.


#### Source

A list of accepted audio sources including the microphone. This is used for ensuring that the audio source specified is valid as well as making the addition of new sources as simple as possible.


To add new streaming source, you have to find a server url which is streaming audio from the desired source. Usually, this involves finding an online radio broadcast then using the Chrome (or your preferred browser) developer console to find the server url of the network request. [TuneIn](https://tunein.com/) is generally a good website for finding new streaming sources. Once you have the appropriate url you simply add the source name and url to the Source enum in the following format and the source is ready to be used.

```

SOURCENAME={

'url': 'url'

}

```

Note that some of the url's in the Source enum may change over time which would cause the corresponding source to stop working. To remedy this you may have to find a new server streaming the source.


To add a new type of source, do the following:

1. Add source to Source enum either with a unique string or a dictionary containing relevant information

2. If applicable, add input validation in `MatchmakerPipeline`'s `validateInput()` method.

3. Create needed filters *only after making sure they don't already exist* (ex. you should use the `ClaimFilterer` rather than writing a new filter for ClaimBuster filtering). All filters created should follow the [guidelines for filters](#threading.thread) discussed in the introduction.

4. In `MatchmakerPipeline`, create an initialize[Source] method in which you initialize all needed queues and filters. Make sure that the final filter writes to the `outQueue`. All filters should be added to the `self.children` array.

5. Finally, add a condition to the `if` statement of the `init` method of `MatchmakerPipeline`, which calls your newly created initialize method if `source` is of the appropriate type


The above steps are the only changes you should need. Specifically, you should not need to touch the `run()`, `flush()`, or `gracefulKill()` methods of `MatchmakerPipeline`. Be careful of editing pre-existing filters since they may be used by other sources.


#### ./resources

ffmpeg (see [AudioConverter](#audioconverter)) is a command line tool and thus takes input and returns output in the form of files. Therefore, any audio to be converted must be written to a file, converted using ffmpeg, and read from the resulting file. These files are deleted from the system as soon as they are no longer needed but if the module is interrupted unnaturally (ex. ```ctrl-c```) these files may persist. The resources subdirectory exists to keep these temporary files separate as well as to make it easy to delete any files which were not deleted by the program due to forced termination. This folder is git ignored. If for some reason this folder is not created for you when the setup script is run, you should make this folder yourselfesources subdirectory exists to keep these temporary files separate as well as to make it easy to delete any files which were not deleted by the program due to forced termination. This folder is git ignored. If for some reason this folder is not created for you when the setup script is run, you should make this folder yourself

## Running the pipeline
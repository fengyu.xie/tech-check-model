from enum import Enum

'''
enum of soure types
'''
class Source(Enum):
    MICROPHONE='MICROPHONE'
    YOUTUBE='YOUTUBE'
    BBC='BBC'
    TEXT='TEXT'

    #streaming sources
    #the url is the url to which a streaming post request is made to get audio
    #to add new streaming sources find an online radio stream and view the network calls
    #note that these urls may need to be updated
    WUNC={
        'url': 'https://mediaserver.wuncfm.unc.edu:9000/wunc64'
    }
    CNN={
        'url': 'http://tunein.streamguys1.com/cnn'
    }
    FOX={
        'url': 'https://streaming-ent.shoutcast.com/foxnews'
    }
    MSNBC={
        'url':'http://tunein.streamguys1.com/secure-msnbc-free?key=e9ee5c690134f8d305a91d448eea53c82a617d84614ff37acba92d951cb9ad83'
    }
    BLOOMBERG={
        'url':'http://tunein.streamguys1.com/bloomberg991'
    }
    CSPAN={
        'url':'http://16083.live.streamtheworld.com:3690/CSPANRADIO_SC'
    }
    AP={
        'url':'http://apnews.streamguys1.com/apnews'
    }
    CNNINTL={
        'url':'http://tunein.streamguys1.com/secure-CNNi?key=7d5555ad0126dcd0fec9730d6e761fe67d520d8de3591d64e53c9d79ab5b3a86'
    }
    HLN={
        'url':'http://tunein.streamguys1.com/cnnhln'
    }
import requests

HOST = 'http://localhost:5000'

class Connector():

    def handleError(self, raw_response):
        print("HTTP Status:", raw_response.status_code)
        if raw_response.status_code == 400:
            print(raw_response.json())

    def insertMedia(self, audio_file, media_url, short_description = None, notes = None):
        url = HOST + '/media/insert'
        data = {
            'audio_file': audio_file,
            'url': media_url,
            'short_description': short_description,
            'notes': notes
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return None
        response = raw_response.json()
        return response['mid']

    def beginProcessingMedia(self, mid):
        url = HOST + '/media/begin'
        data = {
            'mid': mid
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return False
        return True

    def endProcessingMedia(self, mid):
        url = HOST + '/media/end'
        data = {
            'mid': mid
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return False
        return True

    def insertMediaFactsPre(self, mid, fid, notes = None):
        url = HOST + '/media/factsPre/insert'
        data = {
            'mid': mid,
            'fid': fid,
            'notes': notes
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return False
        return True

    def insertMediaTextAlts(self, mid, begins, ends, text, auto_score = None, manual_flag = None, auto_notes = None, manual_notes = None):
        url = HOST + '/media/textAlts/insert'
        data = {
            'mid': mid,
            'begins': begins,
            'ends': ends,
            'text': text,
            'auto_score': auto_score,
            'manual_flag': manual_flag,
            'auto_notes': auto_notes,
            'manual_notes': manual_notes
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return None
        response = raw_response.json()
        return response['mid'], response['mtid']

    def updateClaimBusterScore(self, mid, mtid, score):
        url = HOST + '/media/textAlts/c_score/update'
        data = {
            'mid': mid,
            'mtid': mtid,
            'score': score,
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return False
        return True

    def insertMediaClaimsManual(self, mid, mtid, t, message = None, notes = None):
        url = HOST + '/media/claimsManual/insert'
        data = {
            'mid': mid,
            'mtid': mtid,
            'type': t,
            'message': message,
            'notes': notes
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return False
        return True

    def insertMediaFactsManual(self, mid, mtid, fid, t_flag, r_flag, notes = None):
        url = HOST + '/media/factsManual/insert'
        data = {
            'mid': mid,
            'mtid': mtid,
            'fid': fid,
            't_flag': t_flag,
            'r_flag': r_flag,
            'notes': notes
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return False
        return True

    def insertMediaFactsAuto(self, mid, mtid, fid, t_score = None, r_score = None, notes = None):
        url = HOST + '/media/factsAuto/insert'
        data = {
            'mid': mid,
            'mtid': mtid,
            'fid': fid,
            't_score': t_score,
            'r_score': r_score,
            'notes': notes
        }
        raw_response = requests.post(url, data=data)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return False
        return True

    def getFacts(self, numberOfRow = None):
        url = HOST + '/facts/query'
        payload = {'row': numberOfRow}
        raw_response = requests.get(url, params=payload)
        if raw_response.status_code != requests.codes.ok:
            self.handleError(raw_response)
            return None
        response = raw_response.json()
        return response['facts']

def main():
    c = Connector()
    mid = c.insertMedia("b", "b", "c")
    c.beginProcessingMedia(mid)
    c.endProcessingMedia(mid)
    print(mid)

if __name__ == "__main__":
    main()

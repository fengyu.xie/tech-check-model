## Overview

This is the server side and client side API for accessing and updating the tech check database.

The schema I used is from https://drive.google.com/drive/u/1/folders/1WLhjWyrAJrJCvS2qtK5MPEfg_s0pakb1

This folder consists of four files:

databaseConnector: The database connector that contains all the desired operations on the database.

app.py: The file for the server side API. I used Flask to build the Restful API so that the client side can send requests to the server to access and update the database.

config.py: The file that contains the config for app.py

connector.py: The client side API. User can use this API to make the request to server to access and update the database.

## Usage

### Server

First, we need to run it on the server side. Now I just suppose that we already have the database and schema.

We need to install Flask. Simply `pip3 install flask`

Then we can use `python3 app.py` to make it run on the server side.

### Client

On the client side, user needs to install `pip3 install requests`.

Then in the user code, they need to import connector.py and instantiate a Connector. Then they can use the API provided by Connector to access and update the database on the server side.

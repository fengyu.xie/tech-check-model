from flask import Flask, request, jsonify

from databaseConnector import DatabaseConnector

app = Flask('Database App')
app.config.from_object('config.DevelopmentConfig')

dbConnector = DatabaseConnector()

def handleError(message):
    response = jsonify(message)
    response.status_code = 400
    return response
 
@app.route('/')
def hello():
    return 'Hello World!'

@app.route('/table')
def query_table():
    table_name = request.args.get('table')
    dbConnector._testFetch(table_name)
    return "success"

@app.route('/media/insert', methods=['POST'])
def insert_media():
    req_data = request.form
    required_fields = ['audio_file', 'url', 'short_description']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    print(req_data)
    audio_file = req_data['audio_file']
    url = req_data['url']
    short_description = req_data['short_description']
    notes = req_data.get('notes')
    try:
        mid = dbConnector.insertMedia(audio_file, url, short_description, notes)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(mid=mid)

@app.route('/media/begin', methods=['POST'])
def begin_media_processing():
    req_data = request.form
    required_fields = ['mid']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    mid = req_data['mid']
    try:
        mid = dbConnector.beginProcessingMedia(mid)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(success=True)

@app.route('/media/end', methods=['POST'])
def end_media_processing():
    req_data = request.form
    required_fields = ['mid']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    mid = req_data['mid']
    try:
        mid = dbConnector.endProcessingMedia(mid)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(success=True)

@app.route('/media/factsPre/insert', methods=['POST'])
def insert_media_facts_pre():
    req_data = request.form
    required_fields = ['mid', 'fid']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    mid = req_data['mid']
    fid = req_data['fid']
    notes = req_data.get('notes')
    try:
        dbConnector.insertMediaFactsPre(mid, fid, notes)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(success=True)

@app.route('/media/textAlts/insert', methods=['POST'])
def insert_media_text_alts():
    req_data = request.form
    required_fields = ['mid', 'begins', 'ends', 'text']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    mid = req_data['mid']
    begins = req_data['begins']
    ends = req_data['ends']
    text = req_data['text']
    auto_score = req_data.get('auto_score')
    manual_flag = req_data.get('manual_flag')
    auto_notes = req_data.get('auto_notes')
    manual_notes = req_data.get('manual_notes')
    try:
        mid, mtid = dbConnector.insertMediaTextAlts(mid, begins, ends, text, auto_score, manual_flag, auto_notes, manual_notes)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(
        mid=mid,
        mtid=mtid
        )

@app.route('/media/textAlts/c_score/update', methods=['POST'])
def update_claim_buster_score():
    req_data = request.form
    required_fields = ['mid', 'mtid', 'score']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    mid = req_data['mid']
    mtid = req_data['mtid']
    score = req_data['score']

    try:
        dbConnector.updateClaimBusterScore(mid, mtid, score)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(success=True)

@app.route('/media/claimsManual/insert', methods=['POST'])
def insert_media_claims_manual():
    req_data = request.form
    required_fields = ['mid', 'mtid', 'type']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    mid = req_data['mid']
    mtid = req_data['mtid']
    t = req_data['type']
    message = req_data.get('message')
    notes = req_data.get('notes')

    try:
        dbConnector.insertMediaClaimsManual(mid, mtid, t, message, notes)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(success=True)

@app.route('/media/factsAuto/insert', methods=['POST'])
def insert_media_facts_auto():
    req_data = request.form
    required_fields = ['mid', 'mtid', 'fid']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    mid = req_data['mid']
    mtid = req_data['mtid']
    fid = req_data['fid']
    t_score = req_data.get('t_score')
    r_score = req_data.get('r_score')
    notes = req_data.get('notes')

    try:
        dbConnector.insertMediaFactsAuto(mid, mtid, fid, t_score, r_score, notes)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(success=True)

@app.route('/media/factsManual/insert', methods=['POST'])
def insert_media_facts_manual():
    req_data = request.form
    required_fields = ['mid', 'mtid', 'fid', 't_flag', 'r_flag']
    for required_field in required_fields:
        if required_field not in req_data:
            return handleError('missing ' + required_field + ' in the request')

    mid = req_data['mid']
    mtid = req_data['mtid']
    fid = req_data['fid']
    t_flag = req_data['t_flag']
    r_flag = req_data['r_flag']
    notes = req_data.get('notes')

    try:
        dbConnector.insertMediaFactsManual(mid, mtid, fid, t_flag, r_flag, notes)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(success=True)

@app.route('/facts/query')
def query_facts():
    numberOfRow = request.args.get('row')
    try:
        res = dbConnector.getFacts(numberOfRow)
    except Exception as e:
        dbConnector.commit()
        print(e)
        return handleError(str(e))
    return jsonify(facts=res)
 
if __name__ == '__main__':
    app.run()
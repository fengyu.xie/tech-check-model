import psycopg2
import time

HOST = 'localhost'
DBNAME = 'factcheck'
USERNAME = 'postgres'
PASSWORD = ''

class DatabaseConnector():
    def __init__(self):
        self.connect()
        # self.cursor.execute("SET search_path TO facts;")
        # self.cleanTables()

    def connect(self):        
        conn_string = "host='%s' dbname='%s' user='%s' password='%s'"%(HOST, DBNAME, USERNAME, PASSWORD)
        self.conn = psycopg2.connect(conn_string)
        self.cursor = self.conn.cursor()

    def disconnect(self):
        self.cursor.close()
        self. commit()
        self.conn.close()

    def commit(self):
        self.conn.commit()

    def insertMedia(self, audio_file, url, short_description, notes = None):
        self.cursor.execute("INSERT INTO facts.media (audio_file, url, short_description, timestamp, processing_began, processing_ended, notes) VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING mid", (audio_file, url, short_description, psycopg2.TimestampFromTicks(time.time()), None, None, notes))
        mid = self.cursor.fetchone()[0]
        self.commit()
        return mid

    def beginProcessingMedia(self, mid):
        self.cursor.execute("UPDATE facts.media SET processing_began=%s WHERE mid=%s", (psycopg2.TimestampFromTicks(time.time()), mid))
        self.commit()

    def endProcessingMedia(self, mid):
        self.cursor.execute("UPDATE facts.media SET processing_ended=%s WHERE mid=%s", (psycopg2.TimestampFromTicks(time.time()), mid))
        self.commit()

    def insertMediaFactsPre(self, mid, fid, notes = None):
        self.cursor.execute("INSERT INTO facts.media_facts_pre (mid, fid, notes) VALUES (%s, %s, %s)", (mid, fid, notes))
        self.commit()

    def insertMediaTextAlts(self, mid, begins, ends, text, auto_score = None, manual_flag = 'N', auto_notes = None, manual_notes = None):
        self.cursor.execute("INSERT INTO facts.media_text_alts (mid, begins, ends, text, auto_score, auto_c_score, manual_flag, auto_notes, manual_notes) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING mid, mtid", (mid, begins, ends, text, auto_score, 0, manual_flag, auto_notes, manual_notes))
        res = self.cursor.fetchone()
        print(res)
        res_mid = res[0]
        res_mtid = res[1]
        print(res_mtid)
        self.commit()
        return res_mid, res_mtid

    def updateClaimBusterScore(self, mid, mtid, score):
        self.cursor.execute("UPDATE facts.media_text_alts SET auto_c_score=%s WHERE mid=%s AND mtid=%s", (score, mid, mtid))
        self.commit()

    def insertMediaClaimsManual(self, mid, mtid, t, message = None, notes = None):
        self.cursor.execute("INSERT INTO facts.media_claims_manual (mid, mtid, type, message, timestamp, notes) VALUES (%s, %s, %s, %s, %s, %s)", (mid, mtid, t, message, psycopg2.TimestampFromTicks(time.time()), notes))
        self.commit()

    def insertMediaFactsManual(self, mid, mtid, fid, t_flag, r_flag, notes = None):
        self.cursor.execute("INSERT INTO facts.media_facts_manual (mid, mtid, fid, t_flag, r_flag, notes) VALUES (%s, %s, %s, %s, %s, %s)", (mid, mtid, fid, t_flag, r_flag, notes))
        self.commit()

    def insertMediaFactsAuto(self, mid, mtid, fid, t_score = None, r_score = None, notes = None):
        self.cursor.execute("INSERT INTO facts.media_facts_auto (mid, mtid, fid, t_score, r_score, notes) VALUES (%s, %s, %s, %s, %s, %s)", (mid, mtid, fid, t_score, r_score, notes))
        self.commit()

    def getFacts(self, numberOfRow = None):
        sql_query = "SELECT * FROM facts.facts"
        if numberOfRow is not None:
            sql_query += " limit " + str(numberOfRow)
        self.cursor.execute(sql_query)
        return self.cursor.fetchall()
        
    def _executeCommands(self, commands):
        for command in commands:
            self.cursor.execute(command)
        self.commit()

    def _testFetch(self, tableName):
        self.cursor.execute("SELECT * FROM facts.%s"%(tableName))
        res = self.cursor.fetchall()
        for row in res:
            print(row)

    def _testMedia(self):
        print("\n\ntesting media table...")
        mid = self.insertMedia("a", "b", "c")
        self.beginProcessingMedia(mid)
        self.endProcessingMedia(mid)
        self._testFetch("media")
        return mid

    def _testMediaTextAlts(self, mid):
        print("\n\ntesting media text alts table...")
        (m,t) = self.insertMediaTextAlts(mid,1,2,"a",0.5, "Y")
        print(m,t)
        self.updateClaimBusterScore(m,t,0.5)
        self._testFetch("media_text_alts")
        return m,t

    def cleanTables(self):
        commands = [
            'delete from facts.media_facts_manual',
            'delete from facts.media_facts_auto',
            'delete from facts.media_claims_manual',
            'delete from facts.media_text_alts',
            'delete from facts.media_facts_pre',
            'delete from facts.media'
        ]
        self._executeCommands(commands)

def main():
    c = DatabaseConnector()
    mid = c._testMedia()
    m,t = c._testMediaTextAlts(mid)

    # res = c.getFacts()
    # for row in res:
    #     print(row)

if __name__ == "__main__":
    main()
